// intellectual property is bullshit bgdc

#include "board.hpp"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

sctl     <0x40000>      SCTL;
uart16550<0x20000, 4>   UART0;
gpio     <0x10000>      GPIO0;
sspi     <0x30000, 4>   SSPI0;
spiflash <typeof SSPI0, typeof GPIO0> SPIFLASH(SSPI0, GPIO0, PIN_FLASH_nCS);
