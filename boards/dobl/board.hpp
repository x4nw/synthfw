// intellectual property is bullshit bgdc

#ifndef BOARD_HPP
#define BOARD_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#include "sctl.hpp"
#include "uart16550.hpp"
#include "gpio.hpp"
#include "sspi.hpp"
#include "spiflash.hpp"

extern sctl     <0x40000>      SCTL;
extern uart16550<0x20000, 4>   UART0;
extern gpio     <0x10000>      GPIO0;
extern sspi     <0x30000, 4>   SSPI0;
extern spiflash <typeof SSPI0, typeof GPIO0> SPIFLASH;

#define F_PER 64000000uL
#define F_BAUD  125000uL

#define EXT0  ((uint8_t volatile *) 0x80000)
#define EXT1  ((uint8_t volatile *) 0xC0000)

#define SCTL_TIMER_IRQ 0x8000
#define SCTL_GPIO0_IRQ 0x4000
#define SCTL_UART0_IRQ 0x2000
#define SCTL_SPI0_IRQ  0x1000
#define SCTL_SYSCFG_DBG_bp     8
#define SCTL_SYSCFG_DBG_bm     0x7F00
#define SCTL_SYSCFG_DBG_STROBE 0x8000

#define PIN_NIRQ      0x00000001
#define PIN_FLASH_nCS 0x00000002
#define PIN_WDI       0x00000004
#define PIN_FP_SCL    0x00000008
#define PIN_FP_SDA    0x00000010
#define PIN_FP_NRST   0x00000020
#define PIN_FP_NINT   0x00000040

#endif // !defined(BOARD_HPP)
