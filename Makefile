.PHONY: syn_all clean test

# Specific build targets
.PHONY: run_dobl    # Run the dobl bootloader in Verilator

BUILD = meson compile -C build

syn_all: build
	${BUILD} dobl_quick_tb.fst dobl.multibit

run_dobl: build
	${BUILD} dobl_bootloader_test.fst

test: build
	meson test -C build

clean:
	${RM} -r build
	${RM} meson.build

build: meson.build
	meson setup --cross-file cross-riscv.txt $@

meson.build: meson.build.in meson_soccer/meson.build
	cat $^ > $@

flash: build/dobl.bit
	# Connect a programming cable to the programming port on the board,
	# making sure it shorts RSTREQ to ground to hold the FPGA in reset.
	minipro -p W25Q80DV@SOIC8 -w $< -u -s
