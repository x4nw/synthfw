#!/usr/bin/env python3

# Quick tester to send RSI messages
# Usage: rsitest.py DEVICE ADDR COMMAND ty:value...
#
# DEVICE is formatted with a protocol, a colon, and a path, like:
#   serial:/dev/ttyUSB0     - Direct serial attached to the bus
#   socket:localhost:1000   - A socket interface to the Verilator sim
#   scmserial:/dev/ttyUSB0  - Serial interface attached to the SCM
#   echo                    - Emit the packet that would be sent to the console
#
# ADDR can be an integer or one of the special address codes:
#  geo   (0x00 = geographical direct)
#  geob  (0x01 = geographical broadcast)
#  reply (0xFE = reply)
#  broad (0xFF = broadcast)
#
# COMMAND can be am integer encoding a command or one of the command names
# from the documentation
#
# Values are specified as ty:value where ty selects a type:
#  void
#  ack
#  bool    value should be "true" or "1", or "false" or "0"
#  byte
#  int
#  data    value is hex encoded
#  str     value is UTF-8
#  iarr    comma-separated list of integers

import atexit
import argparse
import binascii
import socket
import struct
import sys
import time

ADDRS = {
    "geo":   0x00,
    "geob":  0x01,
    "reply": 0xFE,
    "broad": 0xFF,
}

COMMANDS = {
    "gen_ping":     (0x0000, ['ack']),
    "gen_addr_dir": (0x0001, ['ack']),
    "gen_addr_brd": (0x0002, ['ack']),
    "gen_dsc_read": (0x0010, ['data']),
    "gen_error":    (0x00FE, ['int', 'str']),
    "gen_response": (0x00FF, None),
    "btl_read":     (0x0100, ['data']),
    "btl_write":    (0x0101, ['ack']),
    "btl_erase":    (0x0102, ['ack']),
    "btl_verify":   (0x0103, ['ack']),
    "btl_cksum":    (0x0104, ['int']),
    "btl_boot":     (0x0110, ['ack']),
}

class UserError(Exception):
    pass

class ProtEcho:
    def __init__(self, arg):
        pass

    def send(self, packet):
        print(" ".join(f"{i:02X}" for i in encode_hdlc(packet)))

    def recv(self):
        return b""

    def shutdown(self):
        pass

class ProtSocket:
    def __init__(self, arg):
        host, _, port = arg.partition(":")
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(1500)
        self.s.connect((host, int(port)))

    def send(self, packet):
        data = bytes(encode_hdlc(packet))
        self.s.sendall(data)

    def recv(self):
        decoder = HdlcDecoder()
        while True:
            data = decoder(self.s.recv(1))
            if data:
                return data

    def shutdown(self):
        self.s.shutdown(socket.SHUT_WR)

class RsiClient:
    def __init__(self):
        pass

    def init_device(self, arg):
        protocol, _, arg = arg.partition(":")
        if protocol == "echo":
            self.device = ProtEcho(arg)
        elif protocol == "socket":
            self.device = ProtSocket(arg)
        else:
            raise UserError(f"Unknown protocol: {protocol}")

        atexit.register(self.device.shutdown)

    def set_address(self, addr_string):
        named = ADDRS.get(addr_string.lower())
        if named is not None:
            self.addr = named
        else:
            v = int(addr_string, 0)
            if not (0 <= v <= 255):
                raise UserError("address out of range")
            self.addr = v

    def do(self, cmd, values):
        cmd, rsp_types = parse_command(cmd)
        values  = [encode_value(i) for i in values]

        parts = [struct.pack(">BH", self.addr, cmd)] + values
        block = b"".join(parts)

        crc = crc16_ccitt(0xFFFF, block)

        block += struct.pack("<H", crc)

        self.device.send(block)

        if self.addr == 0xFF or self.addr == 0x01:
            print("Broadcast address, automatically not waiting for response")
            return

        response = self.device.recv()
        print("Raw payload:", binascii.hexlify(response).decode("ascii"))

        if len(response) < 3:
            print("Packet too short to decode")
            return

        r_add = response[0]
        r_grp = response[1]
        r_cmd = response[2]
        r_grp_cmd = (r_grp << 8) | r_cmd

        if r_add != 0xFE:
            print("Unexpected return target: {r_add:02X}")
        if r_grp_cmd == 0x00FE:
            print("Error")
        elif r_grp_cmd == 0x00FF:
            values = []
            response = response[3:]
            if rsp_types is not None:
                for ty in rsp_types:
                    response, val = decode_value(ty, response)
                    values.append(val)
            print("Response:", ", ".join(values))
        else:
            print(f"Unexpected return message type: {r_grp_cmd:04X}")


def parse_command(arg):
    named = COMMANDS.get(arg.lower())
    if named is not None:
        return named
    v = int(arg, 0)
    if not (0 <= v <= 0xFFFF):
        raise UserError("command out of range")
    return (v, None)

def encode_value(arg):
    ty, _, arg = arg.partition(":")
    if ty == "void":
        return b""
    elif ty == "ack":
        return b""
    elif ty == "bool":
        v = {"false": b"\x00", "0": b"\x00", "true": b"\x01", "1": b"\x01"}.get(
                arg.lower()
        )
        if v is None:
            raise UserError("bool: expected true, false, 1, or 0")
        return v
    elif ty == "byte":
        v = int(arg, 0)
        return struct.pack("<B", v)
    elif ty == "int":
        v = int(arg, 0)
        return struct.pack("<i", v)
    elif ty == "data":
        v = binascii.unhexlify(arg.encode("ascii"))
        if len(v) > 255:
            raise UserError("data: block too long (max 255)")
        return bytes([len(v)]) + v
    elif ty == "str":
        v = arg.encode("utf8")
        if len(v) > 255:
            raise UserError("str: block too long (max 255)")
        return bytes([len(v)]) + v
    elif ty == "iarr":
        v = [int(i, 0) for i in arg.split(",")]
        if len(v) > 255:
            raise UserError("iarr: block too long (max 255)")
        return bytes([len(v)]) + b"".join(struct.pack("<i", i) for i in v)

def decode_value(ty, resp):
    """Try to decode a value of type `ty` off the head of `resp`. Returns the
    remaining bytes of the response and the decoded value string."""

    if ty == "void" or ty == "ack":
        return resp, ty
    elif ty == "data":
        data_len = resp[0]
        data = resp[1:data_len + 1]
        return resp[data_len + 1:], "data:"+binascii.hexlify(data).decode("ascii")
    elif ty == "str":
        data_len = resp[0]
        data = resp[1:data_len + 1]
        data_str = data.decode("utf8")
        return resp[data_len + 1:], "str:"+repr(data_str)
    elif ty == "int":
        data = resp[:4]
        value = struct.unpack("<i", data)[0]
        return resp[4:], "int:"+str(value)
    else:
        return resp, "unexpected type"

def crc16_ccitt(crc, data):
    msb = crc >> 8
    lsb = crc & 255
    for c in data:
        x = c ^ msb
        x ^= (x >> 4)
        msb = (lsb ^ (x >> 3) ^ (x << 4)) & 255
        lsb = (x ^ (x << 5)) & 255
    return (msb << 8) + lsb

def encode_hdlc(block):
    yield 0x7E
    for i in block:
        if i == 0x7D or i == 0x7E:
            yield 0x7D
            yield i ^ 0x20
        else:
            yield i
    yield 0x7E

class HdlcDecoder:
    def __init__(self):
        self.escape = False
        self.data = []

    def __call__(self, data):
        """Consume some bytes. If data is ready, return it."""

        for b in data:
            if b == 0x7E:
                if self.data:
                    return bytes(self.data)
            elif b == 0x7D:
                self.escape = True
            else:
                if self.escape:
                    b ^= 0x20
                    self.escape = False
                self.data.append(b)

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--device", help="serial device")
ap.add_argument("-a", "--addr", help="target address")
ap.add_argument("-A", "--auto-addr", action="store_true",
                help="automatically address a single device")
ap.add_argument("cmd", help="command")
ap.add_argument("args", nargs="*")
args = ap.parse_args()

rsi_client = RsiClient()
rsi_client.init_device(args.device)

if args.auto_addr:
    rsi_client.set_address("broad")
    rsi_client.do("gen_addr_dir", ["byte:2"])
    rsi_client.set_address("2")
else:
    rsi_client.set_address(args.addr)

if args.cmd == "bootpack":
    with open(args.args[0]) as f:
        for line in f:
            if line.startswith("#"):
                continue
            elif line.startswith("E "):
                # erase
                cmd, addr_s, size_s = line.split()
                addr = int(addr_s, 16)
                size = int(size_s, 16)
                print(f"Erase 0x{addr:X}, 0x{size:X}")
                rsi_client.do("btl_erase", [f"int:0x{addr:X}", f"int:0x{size:X}"])
            elif line.startswith("W "):
                cmd, addr_s, data_s = line.split()
                addr = int(addr_s, 16)
                print(f"Write 0x{addr:X}")
                rsi_client.do("btl_write", [f"int:0x{addr:X}", "data:"+data_s])
            elif line.startswith("C "):
                # check
                pass
else:
    rsi_client.do(args.cmd, args.args)
