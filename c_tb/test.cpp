#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <iostream>

#include "rsi.hpp"

void rsi_transmit_byte(uint8_t data)
{
	printf("%02X", data);
}

void rsi_enable_rx(void)
{
}

uint16_t rsi_crc16_ccitt_update(uint16_t crc, uint8_t b)
{
	//fprintf(stderr, "crc: crc = %04X, b = %02X, ", crc, b);

	// From the same CRC code generator as the Verilog module
	// https://bues.ch/cms/hacking/crcgen.html

#define b(x, b) (((x) >> (b)) & 1u)
	uint16_t ret;
	ret  = (uint16_t)(b(crc, 8) ^ b(crc, 12) ^ b(b, 0) ^ b(b, 4)) << 0;
	ret |= (uint16_t)(b(crc, 9) ^ b(crc, 13) ^ b(b, 1) ^ b(b, 5)) << 1;
	ret |= (uint16_t)(b(crc, 10) ^ b(crc, 14) ^ b(b, 2) ^ b(b, 6)) << 2;
	ret |= (uint16_t)(b(crc, 11) ^ b(crc, 15) ^ b(b, 3) ^ b(b, 7)) << 3;
	ret |= (uint16_t)(b(crc, 12) ^ b(b, 4)) << 4;
	ret |= (uint16_t)(b(crc, 8) ^ b(crc, 12) ^ b(crc, 13) ^ b(b, 0) ^ b(b, 4) ^ b(b, 5)) << 5;
	ret |= (uint16_t)(b(crc, 9) ^ b(crc, 13) ^ b(crc, 14) ^ b(b, 1) ^ b(b, 5) ^ b(b, 6)) << 6;
	ret |= (uint16_t)(b(crc, 10) ^ b(crc, 14) ^ b(crc, 15) ^ b(b, 2) ^ b(b, 6) ^ b(b, 7)) << 7;
	ret |= (uint16_t)(b(crc, 0) ^ b(crc, 11) ^ b(crc, 15) ^ b(b, 3) ^ b(b, 7)) << 8;
	ret |= (uint16_t)(b(crc, 1) ^ b(crc, 12) ^ b(b, 4)) << 9;
	ret |= (uint16_t)(b(crc, 2) ^ b(crc, 13) ^ b(b, 5)) << 10;
	ret |= (uint16_t)(b(crc, 3) ^ b(crc, 14) ^ b(b, 6)) << 11;
	ret |= (uint16_t)(b(crc, 4) ^ b(crc, 8) ^ b(crc, 12) ^ b(crc, 15) ^ b(b, 0) ^ b(b, 4) ^ b(b, 7)) << 12;
	ret |= (uint16_t)(b(crc, 5) ^ b(crc, 9) ^ b(crc, 13) ^ b(b, 1) ^ b(b, 5)) << 13;
	ret |= (uint16_t)(b(crc, 6) ^ b(crc, 10) ^ b(crc, 14) ^ b(b, 2) ^ b(b, 6)) << 14;
	ret |= (uint16_t)(b(crc, 7) ^ b(crc, 11) ^ b(crc, 15) ^ b(b, 3) ^ b(b, 7)) << 15;

	//fprintf(stderr, "out = %04X\n", ret);
	return ret;
}

void rsi_disable_irq()
{
}

void rsi_enable_irq()
{
}

int main(int argc, char ** argv)
{
	uint8_t bytes[argc - 1];

	for (int i = 1; i < argc; i++)
	{
		unsigned b;
		int n = sscanf(argv[i], "%02X", &b);
		if (!n)
		{
			std::cerr << "Invalid argument at [" << i << "]: "
				<< argv[i] << std::endl;
			return 1;
		}
		else bytes[i] = b;
	}

	rsi_init();
	for (int i = 1; i < argc; i++)
	{
		rsi_handle_byte(bytes[i]);
	}
	rsi_process();

	puts("");

	return 0;
}
