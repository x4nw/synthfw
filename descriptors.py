# The device descriptors are stored in, and dumped by, Python. This allows them
# to generate informatino dynamically at build time if needed.
#
# Usage: descriptors.py list|-h|--help
#        descriptors.py NAME
#    >>> import descriptors

import argparse
import struct
import sys

import cbor

VERSION = 1

CAP_BOOTLOADER = 0x0001

DESCRIPTORS = {
    "dualosc": {},
    "dualosc_boot": {
        "VERS": VERSION,
        "CAPS": CAP_BOOTLOADER,
        "SHRT": "dualosc",
        "FWVR": "0.1",
        "NAME": "Digital Dual Oscillator",
    },
}

def rename_kv(s):
    k, _, v = s.partition("=")
    if not k or not v or (_ != "="):
        raise argparse.ArgumentTypeError("Expected: --rename=old=new")
    return k, v

def main():
    ap = argparse.ArgumentParser(description="generate descriptor C/H files")
    ap.add_argument("--list", action="store_true", help="list descriptors")
    ap.add_argument("--header", action="store_true", help="emit header")
    ap.add_argument("--rename", action="append", default=[], type=rename_kv, help="rename a descriptor")
    ap.add_argument("DESCRIPTOR", nargs='*', help="descriptor to emit")
    args = ap.parse_args()

    renames = {}
    for k, v in args.rename:
        renames[k] = v

    if args.list:
        for i in DESCRIPTORS.keys():
            print(f"{i}")
        return

    if args.header:
        print("// This file is autogenerated!")
        print("#ifndef DESCRIPTORS_H")
        print("#define DESCRIPTORS_H 1")
        for i in args.DESCRIPTOR:
            data = cbor.dumps(DESCRIPTORS[i])
            name = renames.get(i, i)

            print(f"extern const uint8_t descriptor_{name}[{len(data)}];")
            print(f"extern const size_t descriptor_{name}_len;")
        print("#endif // DESCRIPTORS_H")

    else:
        print("// This file is autogenerated!")
        print("#include <inttypes.h>")
        print("#include <stdlib.h>")
        for i in args.DESCRIPTOR:
            data = cbor.dumps(DESCRIPTORS[i])
            name = renames.get(i, i)

            print(f"const uint8_t descriptor_{name}[] = {{")
            print("\t" + ", ".join(f"0x{i:02X}" for i in data))
            print("};")
            print(f"const size_t descriptor_{name}_len = {len(data)};")

if __name__ == "__main__":
    main()
