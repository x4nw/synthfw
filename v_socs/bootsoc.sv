// bootsoc is a simple SOC for running bootloaders. It has the following
// features:
//
// - RISC-V CPU
// - 8KB of internal block RAM, preloaded from a file
// - 32-bit GPIO
// - 16550 UART with full control signals
// - Simple SPI controller
// - External bus controller for SRAM
// - System controller with interrupts from peripherals and warmboot control
//
// A 64 MHz clock is expected.
//
// MEMORY MAP
// ----------
// 0000 aaaa aaaa aaaa aaaa  Block RAM (8KB, boot from 0x0)
// 0001 ---- ---- ---- aaaa  GPIO0
// 0010 ---- ---- ---- aaaa  UART0
// 0011 ---- ---- ---- aaaa  SPI0
// 0100 ---- ---- ---- aaaa  SCTL
// 10aa aaaa aaaa aaaa aaaa  EBI0 (external bus, select 0)
// 11aa aaaa aaaa aaaa aaaa  EBI1 (external bus, select 1)
//
// IRQ MAP
// -------
// 15     SCTL timer
// 14     GPIO0
// 13     UART0
// 12     SPI0
// 11..0  External interrupts
//
// SYSCFG MAP
// ----------
// All bits passed to toplevel

module bootsoc
#(
	parameter     INIT_FILE = ""
)
(
	input         i_clk,
	input         i_rst,
	input  [11:0] i_irq,

	output [31:0] o_gpio0_out,
	output [31:0] o_gpio0_dir,
	input  [31:0] i_gpio0_in,

	output        o_uart0_txd,
	input         i_uart0_rxd,
	output        o_uart0_rts,
	input         i_uart0_cts,
	output        o_uart0_dtr,
	input         i_uart0_dsr,
	input         i_uart0_ri,
	input         i_uart0_dcd,

	output        o_spi0_sck,
	output        o_spi0_copi,
	input         i_spi0_cipo,

	output        o_sctl_warmboot_boot,
	output [ 1:0] o_sctl_warmboot_sel,
	output [15:0] o_sctl_syscfg,

	output [17:0] o_ebi_a,
	output [ 7:0] o_ebi_d,
	input  [ 7:0] i_ebi_d,
	output        o_ebi_doe,
	output        o_ebi_nrd,
	output        o_ebi_nwr,
	output        o_ebi_stb0,
	output        o_ebi_stb1
);

localparam ADDR_WIDTH = 20;

// Combined bus
wire [ADDR_WIDTH-1:0] wb_adr;
wire [31:0] wb_dat;
wire [3:0]  wb_sel;
wire        wb_we;
wire        wb_cyc;
reg  [31:0] wb_cpu_dat;
reg         wb_cpu_ack;

// System controls
wire [ 1:0] rst;
wire        cpu_irq;
wire [14:0] irq;
localparam  IRQ_GPIO0 = 14;
localparam  IRQ_UART0 = 13;
localparam  IRQ_SPI0  = 12;
assign      irq[11:0] = i_irq;

// Bus signals for internal peripherals
wire [31:0] wb_dat_bram;
reg         wb_stb_bram;
wire        wb_ack_bram;
wire [31:0] wb_dat_gpio0;
reg         wb_stb_gpio0;
wire        wb_ack_gpio0;
wire [31:0] wb_dat_uart0;
reg         wb_stb_uart0;
wire        wb_ack_uart0;
wire [31:0] wb_dat_spi0;
reg         wb_stb_spi0;
wire        wb_ack_spi0;
wire [31:0] wb_dat_sctl;
reg         wb_stb_sctl;
wire        wb_ack_sctl;
wire [31:0] wb_dat_ebi;
reg         wb_stb_ebi;
wire        wb_ack_ebi;

// Narrowed bus for EBI
wire [ADDR_WIDTH-1:0] bus8_adr;
wire [7:0]  bus8_dat;
wire        bus8_we;
wire        bus8_stb;
wire [7:0]  bus8_rdt;
wire        bus8_ack;

// -- Wishbone interconnect/management ----------------------------------------
reg wb_stb_bad;
reg adr_dec_ready;
reg wb_cpu_ack_reg;
reg adr_dec[6:0];

// Address decode needs to be pipelined or we don't meet timing. This adds a
// single wait state.
always_ff @(posedge i_clk) begin
	adr_dec[0]   <= wb_adr[19:16] == 4'h0;
	adr_dec[1]   <= wb_adr[19:16] == 4'h1;
	adr_dec[2]   <= wb_adr[19:16] == 4'h2;
	adr_dec[3]   <= wb_adr[19:16] == 4'h3;
	adr_dec[4]   <= wb_adr[19:16] == 4'h4;
	adr_dec[5]   <= wb_adr[19:16] >= 4'h5 && wb_adr[19:16] < 4'h8;
	adr_dec[6]   <= wb_adr[19:16] >= 4'h8;
	adr_dec_ready<= wb_cyc;
end

always_comb begin
	wb_cpu_dat =
		(wb_ack_bram ? wb_dat_bram : 32'h0)
		| (wb_ack_gpio0 ? wb_dat_gpio0 : 32'h0)
		| (wb_ack_uart0 ? wb_dat_uart0 : 32'h0)
		| (wb_ack_spi0 ? wb_dat_spi0 : 32'h0)
		| (wb_ack_sctl ? wb_dat_sctl : 32'h0)
		| (wb_ack_ebi ? wb_dat_ebi : 32'h0);
end

always_comb begin
	wb_stb_bram  = adr_dec[0] & adr_dec_ready & wb_cyc;
	wb_stb_gpio0 = adr_dec[1] & adr_dec_ready & wb_cyc;
	wb_stb_uart0 = adr_dec[2] & adr_dec_ready & wb_cyc;
	wb_stb_spi0  = adr_dec[3] & adr_dec_ready & wb_cyc;
	wb_stb_sctl  = adr_dec[4] & adr_dec_ready & wb_cyc;
	wb_stb_bad   = adr_dec[5] & adr_dec_ready & wb_cyc;
	wb_stb_ebi   = adr_dec[6] & adr_dec_ready & wb_cyc;
end

always_comb begin
	wb_cpu_ack = |{
		wb_ack_bram,
		wb_ack_gpio0,
		wb_ack_uart0,
		wb_ack_spi0,
		wb_ack_sctl,
		wb_ack_ebi,
		wb_stb_bad
	};
end

// -- Peripherals -------------------------------------------------------------

ram32 #(.ADDR_WIDTH(13), .INIT_FILE(INIT_FILE))
bram (
	.i_clk(i_clk),
	.i_adr(wb_adr[12:0]),
	.i_dat(wb_dat),
	.i_sel(wb_sel),
	.o_dat(wb_dat_bram),
	.i_we (wb_we),
	.i_stb(wb_stb_bram),
	.o_ack(wb_ack_bram)
);

gpio #(.SYNCHRONIZE(0), .HAS_AUX(32'h0))
gpio0 (
	.i_clk(i_clk),
	.i_rst(rst[0]),
	.i_adr(wb_adr[5:0]),
	.i_dat(wb_dat),
	.o_dat(wb_dat_gpio0),
	.i_we (wb_we),
	.i_sel(wb_sel),
	.i_stb(wb_stb_gpio0),
	.o_ack(wb_ack_gpio0),
	.o_int(irq[IRQ_GPIO0]),

	.i_aux(32'h0),
	.o_aux(),

	.o_out(o_gpio0_out),
	.o_dir(o_gpio0_dir),
	.i_in (i_gpio0_in)
);

`ifdef DPI_UART
wire uart_ack, uart_err;
uart_dpi #(
	.tcp_port(10000),
	.welcome_message("")
)
uart0 (
	.wb_clk_i (i_clk),
	.wb_rst_i (rst[1]),
	.wb_adr_i (wb_adr[4:2]),
	.wb_dat_i (wb_dat[7:0]),
	.wb_dat_o (wb_dat_uart0[7:0]),
	.wb_we_i  (wb_we),
	.wb_stb_i (wb_stb_uart0),
	.wb_cyc_i (wb_cyc),
	.wb_ack_o (uart_ack),
	.wb_err_o (uart_err),
	.wb_sel_i (wb_sel),
	.int_o    (irq[IRQ_UART0])
);
assign wb_ack_uart0 = uart_ack || uart_err;
assign o_uart0_txd = 1'b1;
assign o_uart0_rts = 1'b1;
assign o_uart0_dtr = 1'b1;
assign wb_dat_uart0[31:8] = 24'h0;

`else
uart_top
uart0 (
	.wb_clk_i (i_clk),
	.wb_rst_i (rst[0]),
	.wb_adr_i (wb_adr[4:2]),
	.wb_dat_i (wb_dat[7:0]),
	.wb_dat_o (wb_dat_uart0[7:0]),
	.wb_we_i  (wb_we),
	.wb_stb_i (wb_stb_uart0),
	.wb_cyc_i (wb_cyc),
	.wb_ack_o (wb_ack_uart0),
	.wb_sel_i (4'hF),
	.int_o    (irq[IRQ_UART0]),
	.stx_pad_o(o_uart0_txd),
	.srx_pad_i(i_uart0_rxd),
	.rts_pad_o(o_uart0_rts),
	.cts_pad_i(i_uart0_cts),
	.dtr_pad_o(o_uart0_dtr),
	.dsr_pad_i(i_uart0_dsr),
	.ri_pad_i (i_uart0_ri),
	.dcd_pad_i(i_uart0_dcd)
);
assign wb_dat_uart0[31:8] = 24'h0;
`endif

simple_spi_top
spi0 (
	.clk_i (i_clk),
	.rst_i (!rst[1]), // ugh it's active low
	.cyc_i (wb_cyc),
	.stb_i (wb_stb_spi0),
	.adr_i (wb_adr[3:2]),
	.we_i  (wb_we),
	.dat_i (wb_dat[7:0]),
	.dat_o (wb_dat_spi0[7:0]),
	.ack_o (wb_ack_spi0),
	.inta_o(irq[IRQ_SPI0]),
	.sck_o (o_spi0_sck),
	.mosi_o(o_spi0_copi),
	.miso_i(i_spi0_cipo)
);
assign wb_dat_spi0[31:8] = 24'h0;

sctl #(.RESET_COUNT(64), .N_RESETS(2))
sctl (
	.i_clk(i_clk),
	.i_rst(rst[0]),
	.i_adr(wb_adr[3:0]),
	.i_dat(wb_dat),
	.i_sel(wb_sel),
	.o_dat(wb_dat_sctl),
	.i_we (wb_we),
	.i_stb(wb_stb_sctl),
	.o_ack(wb_ack_sctl),
	.o_int(cpu_irq),
	.i_irq(irq),
	.o_syscfg(o_sctl_syscfg),
	.o_warmboot_boot(o_sctl_warmboot_boot),
	.o_warmboot_sel (o_sctl_warmboot_sel),
	.i_rstreq(i_rst),
	.o_rst   (rst)
);

bus_resizer #(.ADDR_WIDTH(ADDR_WIDTH))
ebi_resizer (
	.i_clk    (i_clk),
	.i_rst    (rst[1]),

	.i_32b_adr(wb_adr),
	.i_32b_dat(wb_dat),
	.i_32b_sel(wb_sel),
	.i_32b_we (wb_we),
	.i_32b_cyc(wb_cyc),
	.i_32b_stb(wb_stb_ebi),
	.o_32b_dat(wb_dat_ebi),
	.o_32b_ack(wb_ack_ebi),

	.o_8b_adr (bus8_adr),
	.o_8b_dat (bus8_dat),
	.o_8b_we  (bus8_we),
	.o_8b_cyc (),
	.o_8b_stb (bus8_stb),
	.i_8b_dat (bus8_rdt),
	.i_8b_ack (bus8_ack)
);
wire   ebi_stb;
// TODO combinational directly to IOB...
assign o_ebi_stb0 = !bus8_adr[18] && ebi_stb;
assign o_ebi_stb1 =  bus8_adr[18] && ebi_stb;
assign o_ebi_a    =  bus8_adr[17:0];
ext_bus #(
	.ADDR_TO_RD_READY(4), // 4 for 55ns, 5 for 70ns (at 64 MHz)
	.RD_HOLD(1),
	.WR_SETUP(1),
	.WR_STROBE(3),        // 3 for 55ns, 4 for 70ns (at 64 MHz)
	.WR_HOLD(1)
)
ext_bus (
	.i_clk(i_clk),
	.i_rst(rst[0]),
	.i_stb(bus8_stb),
	.i_we (bus8_we),
	.i_dat(bus8_dat),
	.o_dat(bus8_rdt),
	.o_ack(bus8_ack),
	.o_ebi_d(o_ebi_d),
	.i_ebi_d(i_ebi_d),
	.o_ebi_doe(o_ebi_doe),
	.o_ebi_nrd(o_ebi_nrd),
	.o_ebi_nwr(o_ebi_nwr),
	.o_ebi_cs (ebi_stb)
);

// -- CPU ---------------------------------------------------------------------

wbserv #(.ADDR_WIDTH(ADDR_WIDTH), .CRCACCEL(1))
wbserv (
	.i_clk(i_clk),
	.i_rst(rst[1]),
	.o_adr(wb_adr),
	.o_dat(wb_dat),
	.o_sel(wb_sel),
	.i_dat(wb_cpu_dat),
	.o_we (wb_we),
	.o_cyc(wb_cyc),
	.i_ack(wb_cpu_ack),
	.i_int(cpu_irq)
);

endmodule
