module dualosc_bootloader_top (
	input clk16,
	output [17:0] a,
	inout  [7:0]  d,
	output        nrd,
	output        nwr,
	output        stb0,
	output        stb1,

	// I2S interface
	output        sdoa,
	input         sdia,
	output        sdob,
	input         sdib,
	output        fs,
	output        bclk,
	output        codec_clk,

	// ADC interface
	output        adc_sclk,
	input         adc_cipo,
	output        adc_copi,
	output        adc_clk,
	output        adc_ncs,

	// RS-485 interface
	output        txd_pad,
	output        txen_pad,
	input         rxd_pad,

	// DCV interface
	input         dcv,

	// Thermistor
	output        temp1,
	input         temp2,

	// Flash
	output        flash_copi,
	input         flash_cipo,
	output        flash_sck,
	inout         flash_ss,

	// GPIOs
	inout         wdi,
	inout         adc_ndrdy,
	inout         adc_nsync,
	inout         fp_scl,
	inout         fp_sda,
	inout         fp_nrst,
	inout         fp_nint,
	inout         ncsa,
	inout         ncsb,
	inout         nattn,
	inout         nirq
);

wire pll_lock;
wire clk;

wire warmboot_boot;
wire [1:0] warmboot_sel;

SB_PLL40_CORE #(
	.DIVF(7'd63), // Fvco = 512 MHz (533-1066 allowed, we're running a touch slow)
	.DIVR(4'd3),
	.DIVQ(3'd2),
	.FILTER_RANGE(3'b100),
	.FEEDBACK_PATH("SIMPLE"),
	.DELAY_ADJUSTMENT_MODE_FEEDBACK("FIXED"),
	.FDA_FEEDBACK(4'b0),
	.DELAY_ADJUSTMENT_MODE_RELATIVE("FIXED"),
	.FDA_RELATIVE(4'b0),
	.SHIFTREG_DIV_MODE(2'b0),
	.PLLOUT_SELECT("GENCLK"),
	.ENABLE_ICEGATE(1'b0)
) pll
(
	.RESETB(1'b1),
	.BYPASS(1'b0),
	.REFERENCECLK(clk16),
	.PLLOUTCORE(clk),
	.LOCK(pll_lock)
);

SB_WARMBOOT sbwb(
	.BOOT(warmboot_boot),
	.S1(warmboot_sel[1]),
	.S0(warmboot_sel[0])
);

wire [31:0] gpio0_out;
wire [31:0] gpio0_dir;
wire [31:0] gpio0_in;

wire txd, rxd, txen, nattn;

wire [ 7:0] o_ebi_d;
wire [ 7:0] i_ebi_d;
wire [ 7:0] o_ebi_doe;

bootsoc #(.INIT_FILE("syn.mem"))
bootsoc (
	.i_clk      (clk),
	.i_rst      (!pll_lock),
	.i_irq      (12'h0),

	.o_gpio0_out(gpio0_out),
	.o_gpio0_dir(gpio0_dir),
	.i_gpio0_in (gpio0_in),

	.o_uart0_txd(txd),
	.i_uart0_rxd(rxd),
	.o_uart0_rts(txen),
	.i_uart0_cts(1'b1),
	//.o_uart0_dtr(),
	.i_uart0_dsr(1'b1),
	.i_uart0_ri (nattn),
	.i_uart0_dcd(1'b1),

	.o_spi0_sck (flash_sck),
	.o_spi0_copi(flash_copi),
	.i_spi0_cipo(flash_cipo),

	.o_sctl_warmboot_boot(warmboot_boot),
	.o_sctl_warmboot_sel (warmboot_sel),

	.o_ebi_a   (a),
	.o_ebi_d   (o_ebi_d),
	.i_ebi_d   (i_ebi_d),
	.o_ebi_doe (o_ebi_doe),
	.o_ebi_nrd (nrd),
	.o_ebi_nwr (nwr),
	.o_ebi_stb0(stb0),
	.o_ebi_stb1(stb1)
);

// GPIOs and unused IOs
assign sdoa = 1'b0;
assign sdob = 1'b0;
assign fs   = 1'b0;
assign bclk = 1'b0;
assign codec_clk = 1'b0;

assign adc_sclk  = 1'b0;
assign adc_copi  = 1'b0;
assign adc_clk   = 1'b0;
assign adc_ncs   = 1'b1;
assign adc_ndrdy = 1'bZ;
assign adc_nsync = 1'b1;

assign temp1    = 1'b0;

icegpio gpio0_00 (clk, gpio0_in[0], gpio0_out[0], gpio0_dir[0], nirq);
icegpio gpio0_01 (clk, gpio0_in[1], gpio0_out[1], gpio0_dir[1], flash_ss);
icegpio gpio0_02 (clk, gpio0_in[2], gpio0_out[2], gpio0_dir[2], wdi);

// Route the UART to the front panel header for easy bench testing
`ifdef FP_UART

assign txd_pad = 1'b1;
assign txen_pad = 1'b0;
assign rxd = fp_sda;
assign nattn = fp_nint;

assign fp_scl   = txd;
assign fp_nrst  = txen;
assign gpio0_in[6:3] = 4'b1111;

`else

assign txd_pad = txd;
assign txen_pad = txen;
assign rxd = rxd_pad;
assign nattn = nattn_pad;

icegpio gpio0_03 (clk, gpio0_in[3], gpio0_out[3], gpio0_dir[3], fp_scl);
icegpio gpio0_04 (clk, gpio0_in[4], gpio0_out[4], gpio0_dir[4], fp_sda);
icegpio gpio0_05 (clk, gpio0_in[5], gpio0_out[5], gpio0_dir[5], fp_nrst);
icegpio gpio0_06 (clk, gpio0_in[6], gpio0_out[6], gpio0_dir[6], fp_nint);

`include "ice40_pintypes.v"
for (genvar gi = 0; gi < 8; gi = gi + 1) begin
	always_comb d[gi] = o_ebi_doe ? o_ebi_d[gi] : 1'bZ;
	always_comb i_ebi_d[gi] = d[gi];
end

`endif


endmodule
