`timescale 1ns/1ns

module quick_tb();

reg clk;

wire [31:0] gpio_in;
wire [31:0] gpio_out;
wire [31:0] gpio_dir;
wire [15:0] syscfg;

bootsoc #(.INIT_FILE("dobl.mem"))
bootsoc (
	.i_clk(clk),
	.i_rst(1'b0),
	.i_irq(12'h0),

	.o_gpio0_out(gpio_out),
	.o_gpio0_dir(gpio_dir),
	.i_gpio0_in (gpio_in),

	.o_uart0_txd(),
	.i_uart0_rxd(1'b1),
	.o_uart0_rts(),
	.i_uart0_cts(1'b1),
	.o_uart0_dtr(),
	.i_uart0_dsr(1'b1),
	.i_uart0_ri (1'b0),
	.i_uart0_dcd(1'b1),

	.o_spi0_sck (),
	.o_spi0_copi(),
	.i_spi0_cipo(1'b0),

	.o_sctl_syscfg(syscfg),
	.o_sctl_warmboot_boot(),
	.o_sctl_warmboot_sel(),

	.o_ebi_a(),
	.o_ebi_d(),
	.i_ebi_d(8'h0),
	.o_ebi_doe(),
	.o_ebi_nrd(),
	.o_ebi_nwr(),
	.o_ebi_stb0(),
	.o_ebi_stb1()
);

for (genvar i = 0; i < 32; i++) begin
	assign gpio_in[i] = gpio_dir[i] ? gpio_out[i] : 1'b0;
end

initial begin
	$dumpfile("dump.fst");
	$dumpvars(0, quick_tb);

	clk = 1'b0;
	forever begin
		#7 clk = ~clk;
		#8 clk = ~clk;
	end
end

initial begin
	#1280000;
	$finish;
end

integer first = 1;
always @(posedge syscfg[15]) begin
	logic [6:0] ch = syscfg[14:8];
	if (first != 0) begin
		$write("DBG OUT: ");
		first = 0;
	end
	$write("%c", ch);
	if (ch == 10) first = 1;
end

endmodule
