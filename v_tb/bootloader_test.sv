// Bootloader running test

`timescale 1ns/1ns

module bootloader_test();

logic _clk;

logic [31:0] gpio_in;
logic [31:0] gpio_out;
logic [31:0] gpio_dir;

logic spi0_sck, spi0_copi, spi0_cipo;
logic sctl_warmboot_boot;
logic [1:0] sctl_warmboot_sel;

logic [17:0] _ebi_a;
logic [ 7:0]  ebi_d;
logic [ 7:0] _o_ebi_d;
logic        o_ebi_doe;
logic _ebi_nrd, _ebi_nwr, ebi_stb0, ebi_stb1;
wire [15:0] syscfg;

bootsoc #(.INIT_FILE("dobl.mem"))
bootsoc (
	.i_clk(_clk),
	.i_rst(1'b0),
	.i_irq(12'h0),

	.o_gpio0_out(gpio_out),
	.o_gpio0_dir(gpio_dir),
	.i_gpio0_in (gpio_in),

	.o_uart0_txd(),
	.i_uart0_rxd(1'b1),
	.o_uart0_rts(),
	.i_uart0_cts(1'b1),
	.o_uart0_dtr(),
	.i_uart0_dsr(1'b1),
	.i_uart0_ri (1'b0),
	.i_uart0_dcd(1'b1),

	.o_spi0_sck (spi0_sck),
	.o_spi0_copi(spi0_copi),
	.i_spi0_cipo(spi0_cipo),

	.o_sctl_syscfg(syscfg),
	.o_sctl_warmboot_boot(sctl_warmboot_boot),
	.o_sctl_warmboot_sel(sctl_warmboot_sel),

	.o_ebi_a(_ebi_a),
	.o_ebi_d(_o_ebi_d),
	.i_ebi_d(ebi_d),
	.o_ebi_doe(o_ebi_doe),
	.o_ebi_nrd(_ebi_nrd),
	.o_ebi_nwr(_ebi_nwr),
	.o_ebi_stb0(ebi_stb0),
	.o_ebi_stb1(ebi_stb1)
);

logic [17:0] ebi_a_qual = _ebi_a & {18{ebi_stb0 || ebi_stb1}};
logic ebi_nrd_qual = (ebi_stb0 || ebi_stb1) ? _ebi_nrd : 1'b1;
logic ebi_nwr_qual = (ebi_stb0 || ebi_stb1) ? _ebi_nwr : 1'b1;
for (genvar gi = 0; gi < 8; gi++)
	always_comb ebi_d[gi] = o_ebi_doe ? _o_ebi_d[gi] : 1'bZ;

for (genvar gi = 0; gi < 32; gi++) begin
	assign gpio_in[gi] = gpio_dir[gi] ? gpio_out[gi] : 1'b0;
end

initial begin
	_clk = 1'b0;
	forever begin
		#7 _clk = ~_clk;
		#8 _clk = ~_clk;
	end
end

initial begin
	$dumpfile("dump.fst");
	// veril. ignores the arguments to $dumpvars. I don't want to dump
	// everything in this testbench, so this test is run --trace-depth=1.
	$dumpvars;
end

always @(posedge _clk) begin
	if (sctl_warmboot_boot == 1'b1) begin
		$display("SCTL: booted image %h", sctl_warmboot_sel);
		$finish;
	end
end

bit _first = 1;
always @(posedge syscfg[15]) begin
	logic [6:0] ch = syscfg[14:8];
	if (_first) begin
		$write("DBG OUT: ");
		_first = 0;
	end
	$write("%c", ch);
	if (ch == 10) _first = 1;
end

// SPI flash stuff
import "DPI-C" function void qspiflashsim_init(
	input unsigned n, input int addr_len, input bit debug
);
import "DPI-C" function void qspiflashsim_load(
	input unsigned n, input unsigned addr, input string fname
);
// QSPI mode is supported by packing four bits into dat and the return
import "DPI-C" function int  qspiflashsim_run (
	input unsigned n, input int csn, input int sck, input int dat
);

`define N_FLASH 0
`define DEBUG_SPI 0
logic _last_sck = 0;
logic _last_ncs = 1;
logic _ncs;
initial begin
	qspiflashsim_init(`N_FLASH, 20, 1);
	qspiflashsim_load(`N_FLASH, 0, "dobl.multibit");
end
always @(posedge _clk) begin
	_ncs = gpio_dir[1] ? gpio_out[1] : 1;
	if (_ncs != _last_ncs && `DEBUG_SPI)
		$write("SPI: nCS = %h\n", _ncs);
	if (spi0_sck && !_last_sck && `DEBUG_SPI)
		$write("SPI: COPI = %h", spi0_copi);
	spi0_cipo <= qspiflashsim_run(
		`N_FLASH,
		_ncs ? 1 : 0,
		spi0_sck ? 0 : 1, // qspiflashsim's clocking is broken. inverting the SPI mode fixes it
		spi0_copi ? 1 : 0
	)[1];
	if (spi0_sck && !_last_sck && `DEBUG_SPI)
		$write(", CIPO = %h\n", spi0_cipo);
	_last_sck <= spi0_sck;
	_last_ncs <= _ncs;
end

endmodule
