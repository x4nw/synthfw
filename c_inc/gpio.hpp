// intellectual property is bullshit bgdc

#ifndef GPIO_HPP
#define GPIO_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <etl/io_port.h>

template <uintptr_t ADDR>
struct gpio
{
	etl::io_port_rw<uint32_t, ADDR + 0x00> DIR;
	etl::io_port_wo<uint32_t, ADDR + 0x04> DIRSET;
	etl::io_port_wo<uint32_t, ADDR + 0x08> DIRCLR;
	etl::io_port_wo<uint32_t, ADDR + 0x0C> DIRTGL;

	etl::io_port_rw<uint32_t, ADDR + 0x10> OUT;
	etl::io_port_wo<uint32_t, ADDR + 0x14> OUTSET;
	etl::io_port_wo<uint32_t, ADDR + 0x18> OUTCLR;
	etl::io_port_wo<uint32_t, ADDR + 0x1C> OUTTGL;

	etl::io_port_ro<uint32_t, ADDR + 0x20> IN;
	etl::io_port_rw<uint32_t, ADDR + 0x24> INTFLAGS;
	etl::io_port_rw<uint32_t, ADDR + 0x28> INTEN_R;
	etl::io_port_rw<uint32_t, ADDR + 0x2C> INTEN_F;
};

#endif // !defined(GPIO_HPP)
