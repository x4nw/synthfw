// intellectual property is bullshit bgdc

#ifndef RSI_BOOTLOADER_HPP
#define RSI_BOOTLOADER_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include "rsi.hpp"

err_t btl_read (uint8_t const * frame, size_t len, uint8_t cmd);
err_t btl_write(uint8_t const * frame, size_t len, uint8_t cmd);
err_t btl_erase(uint8_t const * frame, size_t len, uint8_t cmd);
err_t btl_cksum(uint8_t const * frame, size_t len, uint8_t cmd);
err_t btl_boot (uint8_t const * frame, size_t len, uint8_t cmd);

#endif // !defined(RSI_BOOTLOADER_HPP)
