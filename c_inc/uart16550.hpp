#ifndef UART16550_HPP
#define UART16550_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#include <etl/io_port.h>

// Note that this driver is for our 16550-derived verilog core, not a real
// 16550. They're not identical.

// wtf does EDSSI stand for??
#define UART_IER_EDSSI         0x08 // Enable modem status interrupt
#define UART_IER_ELSI          0x04 // Enable line status interrupt
#define UART_IER_ETBEI         0x02 // Enable transmit buffer empty interrupt
#define UART_IER_ERBI          0x01 // Enable received byte/timeout interrupt

#define UART_IIR_INTID_bp      1
#define UART_IIR_INTID_gm      (7 << UART_IIR_INTID_bp)
// Source: parity, overrun, framing errors; break interrupt
// Clear:  read LSR
#define UART_IIR_INTID_LSI_gc  (3 << UART_IIR_INTID_bp)
// Source: FIFO trigger level reached
// Clear:  FIFO drops below trigger level
#define UART_IIR_INTID_RBI_gc  (2 << UART_IIR_INTID_bp)
// Source: At least 1 char in FIFO, but no reads for 4 char times
// Clear:  Read from FIFO
#define UART_IIR_INTID_TMOI_gc (6 << UART_IIR_INTID_bp)
// Source: Tramitter holding buffer empty
// Clear:  Write to holding buffer or clear IIR
#define UART_IIR_INTID_TBEI_gc (1 << UART_IIR_INTID_bp)
// Source: CTS, DSR, RI, DCD changed
// Clear:  Read MSR
#define UART_IIR_INTID_DSSI_gc (0 << UART_IIR_INTID_bp)

#define UART_IIR_IPEND         0x01 // No interrupt pending

// RX FIFO trigger level
#define UART_FCR_RXFIFTL_bp    6
#define UART_FCR_RXFIFTL_gm    (3 << UART_FCR_RXFIFTL_bp)
#define UART_FCR_RXFIFTL_1_gc  (0 << UART_FCR_RXFIFTL_bp)
#define UART_FCR_RXFIFTL_4_gc  (1 << UART_FCR_RXFIFTL_bp)
#define UART_FCR_RXFIFTL_8_gc  (2 << UART_FCR_RXFIFTL_bp)
#define UART_FCR_RXFIFTL_14_gc (3 << UART_FCR_RXFIFTL_bp)

#define UART_FCR_TXCLR         0x04
#define UART_FCR_RXCLR         0x02
#define UART_FCR_FIFOEN        0x01 // Always set; ignored

#define UART_LCR_WLS_bp          0
#define UART_LCR_WLS_gm          (3 << UART_LCR_WLS_bp)
#define UART_LCR_WLS_5BIT_gc     (0 << UART_LCR_WLS_bp)
#define UART_LCR_WLS_6BIT_gc     (1 << UART_LCR_WLS_bp)
#define UART_LCR_WLS_7BIT_gc     (2 << UART_LCR_WLS_bp)
#define UART_LCR_WLS_8BIT_gc     (3 << UART_LCR_WLS_bp)
#define UART_LCR_STB_bp          2
#define UART_LCR_STB_gm          (1 << UART_LCR_STB_bp)
#define UART_LCR_STB_1_gc        (0 << UART_LCR_STB_bp)
#define UART_LCR_STB_2_gc        (1 << UART_LCR_STB_bp) // 1.5 for WLS_5BIT_gc
#define UART_LCR_PARITY_bp       3
#define UART_LCR_PARITY_gm       (7 << UART_LCR_PARITY_bp)
#define UART_LCR_PARITY_NONE_gc  (0 << UART_LCR_PARITY_bp)
#define UART_LCR_PARITY_ODD_gc   (1 << UART_LCR_PARITY_bp)
#define UART_LCR_PARITY_EVEN_gc  (3 << UART_LCR_PARITY_bp)
#define UART_LCR_PARITY_MARK_gc  (5 << UART_LCR_PARITY_bp)
#define UART_LCR_PARITY_SPACE_gc (7 << UART_LCR_PARITY_bp)
#define UART_LCR_BREAK           0x40
#define UART_LCR_DLA             0x80 // Divisor latch access

#define UART_MCR_DTR             0x01 // Inverted
#define UART_MCR_RTS             0x02 // Inverted
#define UART_MCR_OUT1            0x04 // Connects to RI in loopback
#define UART_MCR_OUT2            0x08 // Connects to DCD in loopback
#define UART_MCR_LOOP            0x10 // Connects to DCD in loopback

#define UART_LSR_DR              0x01 // Data ready
#define UART_LSR_OE              0x02 // Overrun error
#define UART_LSR_PE              0x04 // Parity error
#define UART_LSR_FE              0x08 // Framing error
#define UART_LSR_BI              0x10 // Break interrupt
#define UART_LSR_THRE            0x20 // Transmit holding register empty
#define UART_LSR_TEMT            0x40 // Transmitter empty
#define UART_LSR_RXFIFOE         0x80 // At least one error in the rx fifo

#define UART_MSR_DCTS            0x01 // Delta CTS
#define UART_MSR_DDSR            0x02 // Delta DSR
#define UART_MSR_TERI            0x04 // Trailing edge of RI. low->high
#define UART_MSR_DDCD            0x08 // Delta DCD
#define UART_MSR_CTS             0x10 // Complement of CTS
#define UART_MSR_DSR             0x20 // Complement of DSR
#define UART_MSR_RI              0x40 // Complement of RI
#define UART_MSR_DCD             0x80 // Complement of DCD

template <uintptr_t ADDR, uintptr_t STRIDE>
struct uart16550
{
	// Receive buffer register
	etl::io_port_ro<uint8_t, ADDR + 0 * STRIDE> RBR;
	// Transmit holding register
	etl::io_port_rw<uint8_t, ADDR + 0 * STRIDE> THR;
	// Baud divisor LSB; set DLA to access
	etl::io_port_rw<uint8_t, ADDR + 0 * STRIDE> DIVISOR_LSB;

	// Interrupt enable register
	etl::io_port_rw<uint8_t, ADDR + 1 * STRIDE> IER;
	// Baud divisor MSB; set DLA to access
	etl::io_port_rw<uint8_t, ADDR + 1 * STRIDE> DIVISOR_MSB;

	// Interrupt identification register
	etl::io_port_ro<uint8_t, ADDR + 2 * STRIDE> IIR;
	// FIFO control register
	etl::io_port_rw<uint8_t, ADDR + 2 * STRIDE> FCR;

	// Line control register
	etl::io_port_rw<uint8_t, ADDR + 3 * STRIDE> LCR;

	// Modem control register
	etl::io_port_rw<uint8_t, ADDR + 4 * STRIDE> MCR;

	// Line status register
	etl::io_port_ro<uint8_t, ADDR + 5 * STRIDE> LSR;

	// Modem status register
	etl::io_port_ro<uint8_t, ADDR + 6 * STRIDE> MSR;

	void set_divisor_reg(uint16_t value)
	{
		uint8_t lcr = LCR;
		LCR = lcr | UART_LCR_DLA;
		DIVISOR_MSB = value >> 8;
		DIVISOR_LSB = value & 0xFF;
		LCR = lcr;
	}
};


#endif // !defined(UART16550_HPP)
