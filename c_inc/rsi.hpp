#ifndef RSI_H
#define RSI_H

// RSI = RS-485 Interface

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

typedef uint32_t err_t;

#define ERR_UNKNOWN  0x00000000
#define ERR_SERIAL   0x00010000
#define ERR_CGROUP   0x00020000
#define ERR_CMD      0x00030000
#define ERR_ARGLEN   0x00040000
#define ERR_ARGVAL   0x00050000
#define ERR_CKSUM    0x00060000
#define ERR_SWFAULT  0x00070000
#define ERR_HWFAULT  0x00080000
#define ERR_NOBOOT   0x00090000

#define ADDR_GEO_DIR 0x00
#define ADDR_GEO_BRD 0x01
#define ADDR_REPLY   0xFE
#define ADDR_BRD     0xFF

#define GEN_group    0x00
#define GEN_PING     0x00
#define GEN_ADDR_DIR 0x01
#define GEN_ADDR_BRD 0x02
#define GEN_DSC_READ 0x10
#define GEN_ERROR    0xFE
#define GEN_RESPONSE 0xFF

#define BTL_group    0x01
#define BTL_READ     0x00
#define BTL_WRITE    0x01
#define BTL_ERASE    0x02
#define BTL_CKSUM    0x04
#define BTL_BOOT     0x10


// Only call once! No re-init!
void rsi_init(void);

// Handle a byte received from the UART. Returns whether it's valid to receive
// any more. If this returns false, you MUST NOT pass any more characters in
// until rsi_enable_rx() is called!
bool rsi_handle_byte(uint8_t data);

// Process any RSI message that has been received
void rsi_process(void);

// Must be defined by the lower level driver - transmits a byte.
extern void rsi_transmit_byte(uint8_t data);

// Must be defined by the lower level driver - reënables receiver.
extern void rsi_enable_rx(void);

// Must be defined by the implementer - perform a single CRC operation
extern uint16_t rsi_crc16_ccitt_update(uint16_t crc, uint8_t b);

// Must be defined by the implementer - disable interrupts
extern void rsi_disable_irq();

// Must be defined by the implementer - enable interrupts
extern void rsi_enable_irq();

// ---- internal to rsi ----

// Process a validated packet
void rsi_process_packet(uint8_t const * frame, size_t len);
// Start a packet
void rsi_start_packet(uint8_t dest, uint8_t cgroup, uint8_t cmd);
// Send a bool
#define rsi_send_bool(v) rsi_send_byte((v) ? 1 : 0)
// Send a byte
void rsi_send_byte(uint8_t v);
// Send an int
void rsi_send_int(int32_t v);
// Send a uint
void rsi_send_uint(uint32_t v);
// Send a data block
void rsi_send_data(uint8_t len, uint8_t const * data);
// Send a string
void rsi_send_str(char const * s);
// End the packet
void rsi_end_packet(void);
// Suppress output until the next packet
void rsi_suppress_output(void);

// All type parsers process a value of that type out of the packet, move the
// pointer and len, pass back the value through the outparam, and return zero
// on success or an error code on failure.
//
// (TODO: error code zero isn't actually a reserved value, but it should be)
err_t rsi_parse_byte(uint8_t const ** frame, size_t * len, uint8_t * val);
err_t rsi_parse_int (uint8_t const ** frame, size_t * len, int32_t * val);
err_t rsi_parse_data(uint8_t const ** frame, size_t * len, uint8_t * size, void const ** data);

void rsi_send_error(err_t err);


#ifdef __cplusplus
}
#endif

#endif // !defined(RSI_H)
