#ifndef SYSCTRL_HPP
#define SYSCTRL_HPP


#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#include <etl/io_port.h>

template <uintptr_t ADDR>
struct sctl
{
	etl::io_port_rw<uint16_t, ADDR + 0x00> INTEN;
	etl::io_port_rw<uint16_t, ADDR + 0x02> SYSCFG;
	etl::io_port_rw<uint16_t, ADDR + 0x04> INTFLAGS;
	etl::io_port_rw<uint32_t, ADDR + 0x08> DIVISOR;
	etl::io_port_rw<uint32_t, ADDR + 0x0C> WARMBOOT;

	static constexpr uint32_t WARMBOOT_KEY = 0x5B985D1u << 2u;

	__attribute__((noreturn))
	void warmboot(unsigned image)
	{
		WARMBOOT = WARMBOOT_KEY | (image & 3);
		__builtin_unreachable();
	}
};

#endif // !defined(SYSCTRL_HPP)
