// intellectual property is bullshit bgdc

#ifndef SSPI_HPP
#define SSPI_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <etl/io_port.h>

#define SSPI_SPCR_SPIE 0x80 // Serial Peripheral Interrupt Enable
#define SSPI_SPCR_SPE  0x40 // Serial Peripheral Enable
#define SSPI_SPCR_MSTR 0x10 // Master Mode Select
#define SSPI_SPCR_CPOL 0x08 // Clock Polarity
#define SSPI_SPCR_CPHA 0x04 // Clock Phase
#define SSPI_SPCR_SPR_bp 0  // SPI Clock Rate Select
#define SSPI_SPCR_SPR_gm (3 << SSPI_SPCR_SPR_bp)

#define SSPI_SPSR_SPIF    0x80 // Serial Peripheral Interrupt Flag
#define SSPI_SPSR_WCOL    0x40 // Write Collision
#define SSPI_SPSR_WFFULL  0x08 // Write FIFO Full
#define SSPI_SPSR_WFEMPTY 0x04 // Write FIFO Empty
#define SSPI_SPSR_RFFULL  0x02 // Read FIFO Full
#define SSPI_SPSR_RFEMPTY 0x01 // Read FIFO Empty

#define SSPI_SPER_ICNT_bp   6 // Interrupt Count - determines transfer block size
#define SSPI_SPER_ICNT_gm   (3 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_1_gc (0 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_2_gc (1 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_3_gc (2 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_4_gc (3 << SSPI_SPER_ICNT_bp)

#define SSPI_SPER_ESPR_bp 0 // Extended SPI Clock Rate Select
#define SSPI_SPER_ESPR_gm (3 << SSPI_SPER_ESPR_bp)

// SPI clock rates. These get unpacked into both SPER.ESPR and SPCR.SPR
#define SSPI_CDIV_2    0x0
#define SSPI_CDIV_4    0x1
#define SSPI_CDIV_16   0x2
#define SSPI_CDIV_32   0x3
#define SSPI_CDIV_8    0x4
#define SSPI_CDIV_64   0x5
#define SSPI_CDIV_128  0x6
#define SSPI_CDIV_256  0x7
#define SSPI_CDIV_512  0x8
#define SSPI_CDIV_1024 0x9
#define SSPI_CDIV_2048 0xA
#define SSPI_CDIV_4096 0xB
#define SSPI_SPCR_SPR_CDIV(x)  (((x) & 0x3) << SSPI_SPCR_SPR_bp)
#define SSPI_SPER_ESPR_CDIV(x) ((((x) & 0xC) >> 2) << SSPI_SPER_ESPR_bp)

// Register definitions for the opencores Simple SPI wishbone core
template <uintptr_t ADDR, uintptr_t STRIDE>
struct sspi
{
	etl::io_port_rw<uint8_t, ADDR + 0 * STRIDE> SPCR;
	etl::io_port_rw<uint8_t, ADDR + 1 * STRIDE> SPSR;
	etl::io_port_rw<uint8_t, ADDR + 2 * STRIDE> SPDR;
	etl::io_port_rw<uint8_t, ADDR + 3 * STRIDE> SPER;

	// Initialize in controller mode without interrupts.
	void init(unsigned cdiv, unsigned mode)
	{
		SPER = SSPI_SPER_ESPR_CDIV(cdiv);
		SPCR = SSPI_SPCR_SPR_CDIV(cdiv)
			| SSPI_SPCR_SPE | SSPI_SPCR_MSTR
			| ((mode & 1) ? SSPI_SPCR_CPHA : 0)
			| ((mode & 2) ? SSPI_SPCR_CPOL : 0);
		flush();
	}

	void flush()
	{
		while (!(SPSR & SSPI_SPSR_WFEMPTY));
		while (!(SPSR & SSPI_SPSR_RFEMPTY)) SPDR.read();
	}

	// Perform a transfer. Either or both pointers may be null - if
	// data_in is null, zeros are transmitted. If non-null the arrays must
	// be of size n.
	void transfer(uint8_t * data_in, uint8_t const * data_out, size_t n)
	{
		size_t n_tx = 0, n_rx = 0;
		while (n_tx < n || n_rx < n)
		{
			uint8_t spsr = SPSR;
			if (n_tx < n && !(spsr & SSPI_SPSR_WFFULL))
			{
				uint8_t b = data_out ? data_out[n_tx] : 0;
				n_tx++;
				SPDR = b;
			}

			if (!(spsr & SSPI_SPSR_RFEMPTY))
			{
				uint8_t b = SPDR;
				if (n_rx < n && data_in)
				{
					data_in[n_rx] = b;
				}
				n_rx++;
			}
		}
	}
};

#endif // !defined(SSPI_HPP)
