// Support for hardware accelerators attached to the SERV MDU port. These
// repurpose the multiplication/division instructions.

#ifndef ACCEL_H
#define ACCEL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// Declare that accelerators are used in a file. This enables the
// multiplication/division opcodes at the assembler level, letting the C
// compiler continue to believe it cannot emit them.
#define ACCEL_ENABLE asm (".attribute arch, \"rv32im\"")

// Compute CRC16-CCITT using the hardware accelerator.
// Operand is 32-bit to avoid unnecessary truncation instructions, since the
// accelerator ignores the extra bits anyway.
__attribute__((used))
static inline uint32_t crc16_ccitt_update(uint32_t crc, uint8_t b)
{
	asm ("mul %0, %1, %2" : "=r"(crc) : "r"(b), "r"(crc));
	return crc;
}

#ifdef __cplusplus
}
#endif

#endif // !defined(ACCEL_H)
