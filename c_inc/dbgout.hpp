// intellectual property is bullshit bgdc

#ifndef DBGOUT_H
#define DBGOUT_H

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>

// Efficiently emit a hex value with a label. Wastes less space than printf
void dbgout(char const * label, uint32_t val);

// Efficiently emit a hex block with a label. Wastes less space than printf
void dbgout(char const * label, void const * val, size_t n);

// Emit a string
void dbgout(char const * label);

#endif // !defined(DBGOUT_H)
