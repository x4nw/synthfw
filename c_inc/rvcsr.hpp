// intellectual property is bullshit bgdc

#ifndef RVCSR_H
#define RVCSR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#define RV_MSTATUS_MIE 0x08
#define RV_MIE_MTIE    0x80

// All the MCAUSE values SERV can give. Note that it uses "timer" interrupts
// as the sole hardware interrupt source, so RV_MCAUSE_HW_IRQ is really the
// timer interrupt flag.
#define RV_MCAUSE_EBREAK           0x00000003
#define RV_MCAUSE_ECALL            0x0000000B
#define RV_MCAUSE_MISALIGNED_STORE 0x00000006
#define RV_MCAUSE_MISALIGNED_LOAD  0x00000004
#define RV_MCAUSE_MISALIGNED_JUMP  0x00000000
#define RV_MCAUSE_HW_IRQ           0x80000007

#define RV_MSTATUS 0x300
#define RV_MIE     0x304
#define RV_MCAUSE  0x342
#define RV_MTVEC   0x305

// Set bits in a CSR, returning the original value into dest
#define RV_CSRRS(csr, bits, dest) do { \
	asm volatile ("csrrs %0, %1, %2" : "=r"(dest) : "i"(csr), "r"(bits)); \
} while (0)

// Clear bits in a CSR, returning the original value into dest
#define RV_CSRRC(csr, bits, dest) do { \
	asm volatile ("csrrc %0, %1, %2" : "=r"(dest) : "i"(csr), "r"(bits)); \
} while (0)

// Write a CSR, returning the original value into dest
#define RV_CSRRW(csr, bits, dest) do { \
	asm volatile ("csrrw %0, %1, %2" : "=r"(dest) : "i"(csr), "r"(bits)); \
} while (0)

// Read MCAUSE
static inline uint32_t rv_mcause(void)
{
	uint32_t mcause;
	RV_CSRRS(RV_MCAUSE, 0, mcause);
	return mcause;
}

// Set the interrupt handler address
static inline void rv_set_handler(void (*f)(void))
{
	//csrrw_mtvec((uintptr_t) f);
	uint32_t _;
	RV_CSRRW(RV_MTVEC, (uintptr_t) f, _);
}

// Enable exception handling
static inline void rv_exc_enable(void)
{
	uint32_t _;
	RV_CSRRS(RV_MSTATUS, RV_MSTATUS_MIE, _);
}

// Disable exception handling
static inline void rv_exc_disable(void)
{
	uint32_t _;
	RV_CSRRC(RV_MSTATUS, RV_MSTATUS_MIE, _);
}

// Enable interrupt request handling. Exceptions must also be enabled.
static inline void rv_irq_enable(void)
{
	uint32_t _;
	RV_CSRRS(RV_MIE, RV_MIE_MTIE, _);
}

// Disable interrupt request handling.
static inline void rv_irq_disable(void)
{
	uint32_t _;
	RV_CSRRC(RV_MIE, RV_MIE_MTIE, _);
}

#ifdef __cplusplus
}
#endif

#endif // !defined(RVCSR_H)
