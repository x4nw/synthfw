// intellectual property is bullshit bgdc

#ifndef SPIFLASH_HPP
#define SPIFLASH_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include "dbgout.hpp"

// Simple SPI flash chip driver. Template argument is an SPI controller class,
// which should have methods:
//     void transfer(uint8_t * data_in, uint8_t const * data_out, size_t n)
//     void flush()
//
// And a GPIO class.
//
// The SPI and GPIO are not initialized.
template<typename Spi, typename Gpio>
class spiflash
{
public:
	spiflash(Spi & spi, Gpio & gpio, uint32_t gpio_pin)
		: _spi(spi)
		, _gpio(gpio)
		, _gpio_pin(gpio_pin)
	{
	}

	void reset()
	{
		uint8_t cmd[1];

		_spi.flush();

#if 0
		cmd[0] = C_ENABLE_RESET;
		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		cs_negate();

		cmd[0] = C_RESET;
		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		cs_negate();
#else
		cmd[0] = C_CLSR;
		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		cs_negate();
	}
#endif

	// Read the JEDEC ID as {MF7..0 ID15..0}
	// W25Q80DV = 0xEF4014
	uint32_t read_jedec_id()
	{
		uint8_t cmd[4] = {C_READ_JEDEC, 0, 0, 0};

		_wait_ready();

		cs_assert();
		_spi.transfer(cmd, cmd, sizeof cmd);
		cs_negate();

		uint32_t id = cmd[3];
		id |= ((uint32_t) cmd[2]) << 8;
		id |= ((uint32_t) cmd[1]) << 16;
		return id;
	}

	// Read data at once
	void read_data(uint32_t addr, uint8_t * data, size_t n)
	{
		// We're not really running fast enough to require Fast Read,
		// but the testbench SPI flash emulator only supports it.
		uint8_t cmd[5] = {
			C_FAST_READ,
			(uint8_t)(addr >> 16),
			(uint8_t)(addr >> 8),
			(uint8_t)(addr),
			0
		};

		_wait_ready();

		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		_spi.transfer(data, nullptr, n);
		cs_negate();
	}

	// Read data in chunks with a callback
	void read_data_chunked(
		uint32_t addr, uint8_t * buf, size_t buf_size, size_t n,
		void (*cb)(uint8_t const * data, size_t n)
	)
	{
		uint8_t cmd[5] = {
			C_FAST_READ,
			(uint8_t)(addr >> 16),
			(uint8_t)(addr >> 8),
			(uint8_t)(addr),
			0
		};

		_wait_ready();

		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		while (n)
		{
			size_t chunk_size = (n > buf_size) ? buf_size : n;
			_spi.transfer(buf, nullptr, chunk_size);
			cb(buf, chunk_size);
			n -= chunk_size;
		}
		cs_negate();
	}

	// Write a page, up to 256 bytes. Must not wrap around.
	void write_page(uint32_t addr, uint8_t const * data, size_t n)
	{
		uint8_t cmd[4] = {
			C_WRITE_EN,
			(uint8_t)(addr >> 16),
			(uint8_t)(addr >> 8),
			(uint8_t)(addr)
		};

		_wait_ready();

		cs_assert();
		_spi.transfer(nullptr, cmd, 1);
		cs_negate();

		cmd[0] = C_PPROG;
		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		_spi.transfer(nullptr, data, n);
		cs_negate();
	}

	// Erase a 4KB sector
	// Note: not supported by the testbench flash emulator
	void erase_sect_4KB(uint32_t addr)
	{
		_erase_at(addr, C_SECT_ERASE);
	}

	// Erase a 32KB block
	// Note: not supported by the testbench flash emulator
	void erase_block_32KB(uint32_t addr)
	{
		_erase_at(addr, C_BLK_ERASE_32);
	}

	// Erase a 64KB block
	void erase_block_64KB(uint32_t addr)
	{
		_erase_at(addr, C_BLK_ERASE_64);
	}

	// Erase the chip
	void erase_chip()
	{
		uint8_t cmd[1];

		cmd[0] = C_WRITE_EN;
		cs_assert();
		_spi.transfer(nullptr, cmd, 1);
		cs_negate();

		cmd[0] = C_CHIP_ERASE;
		cs_assert();
		_spi.transfer(nullptr, cmd, 1);
		cs_negate();
	}

private:

	void cs_assert()
	{
		_gpio.OUTCLR = _gpio_pin;
	}

	void cs_negate()
	{
		_gpio.OUTSET = _gpio_pin;
	}

	void _erase_at(uint32_t addr, uint8_t cmd_id)
	{
		uint8_t cmd[4] = {
			C_WRITE_EN,
			(uint8_t)(addr >> 16),
			(uint8_t)(addr >> 8),
			(uint8_t)(addr)
		};

		_wait_ready();

		cs_assert();
		_spi.transfer(nullptr, cmd, 1);
		cs_negate();

		cmd[0] = cmd_id;
		cs_assert();
		_spi.transfer(nullptr, cmd, sizeof cmd);
		cs_negate();
	}

	void _wait_ready()
	{
		uint8_t cmd[2] = {C_RSR1, SR1_BUSY};

		while (cmd[1] & SR1_BUSY)
		{
			cmd[0] = C_RSR1;
			cs_assert();
			_spi.transfer(cmd, cmd, sizeof cmd);
			cs_negate();
		}
	}

	static constexpr uint8_t SR1_SRP0 = 0x80; // Status register protect 0
	static constexpr uint8_t SR1_SEC  = 0x40; // Sector protect
	static constexpr uint8_t SR1_TB   = 0x20; // Top/bottom protect
	static constexpr uint8_t SR1_BP2  = 0x10; // Block protect 2
	static constexpr uint8_t SR1_BP1  = 0x08; // Block protect 1
	static constexpr uint8_t SR1_BP0  = 0x04; // Block protect 0
	static constexpr uint8_t SR1_WEL  = 0x02; // Write enable latch
	static constexpr uint8_t SR1_BUSY = 0x01; // Busy
	static constexpr uint8_t SR2_SUS  = 0x80; // Suspend status
	static constexpr uint8_t SR2_CMP  = 0x40; // Complement protect
	static constexpr uint8_t SR2_LB3  = 0x20; // Lock bit 3
	static constexpr uint8_t SR2_LB2  = 0x10; // Lock bit 2
	static constexpr uint8_t SR2_LB1  = 0x08; // Lock bit 1
	static constexpr uint8_t SR2_QE   = 0x02; // Quad enable
	static constexpr uint8_t SR2_SRP1 = 0x02; // Status register protect 1

	static constexpr uint8_t C_WRITE_EN     = 0x06; // Write enable
	static constexpr uint8_t C_WRITE_EN_VSR = 0x50; // Write enable (volatile status reg)
	static constexpr uint8_t C_WRITE_DIS    = 0x04; // Write disable
	static constexpr uint8_t C_RSR1         = 0x05; // Read status reg 1
	static constexpr uint8_t C_RSR2         = 0x35; // Read status reg 2
	static constexpr uint8_t C_WSR          = 0x01; // Write status reg
	static constexpr uint8_t C_READ         = 0x03; // Read data
	static constexpr uint8_t C_FAST_READ    = 0x0B; // Read data, fast
	static constexpr uint8_t C_PPROG        = 0x02; // Page program (256B)
	static constexpr uint8_t C_SECT_ERASE   = 0x20; // Sector erase (4KB)
	static constexpr uint8_t C_BLK_ERASE_32 = 0x52; // Block erase (32KB)
	static constexpr uint8_t C_BLK_ERASE_64 = 0xD8; // Block erase (64KB)
	static constexpr uint8_t C_CHIP_ERASE   = 0xC7; // Chip erase
	static constexpr uint8_t C_SUSPEND      = 0x75; // Erase/prog suspend
	static constexpr uint8_t C_RESUME       = 0x7A; // Erase/prog resume
	static constexpr uint8_t C_PWRDN        = 0xB9; // Power down
	static constexpr uint8_t C_PWRUP_DEVID  = 0xB9; // Power up / device ID
	static constexpr uint8_t C_READ_MFDEVID = 0x90; // Read manuf/dev ID
	static constexpr uint8_t C_READ_UNIQUE  = 0x4B; // Read unique ID
	static constexpr uint8_t C_READ_JEDEC   = 0x9F; // Read JEDEC ID
	static constexpr uint8_t C_READ_SFDP    = 0x5A; // Read SFDP register
	static constexpr uint8_t C_ERASE_SEC    = 0x44; // Erase security regs
	static constexpr uint8_t C_PROG_SEC     = 0x42; // Pgm security regs
	static constexpr uint8_t C_READ_SEC     = 0x42; // Read security regs
	static constexpr uint8_t C_ENABLE_RESET = 0x66; // Enable reset
	static constexpr uint8_t C_RESET        = 0x99; // Reset

	// Clear Status Register is used by the emulated flash, but not
	// supporte by the real flash
	static constexpr uint8_t C_CLSR         = 0x30; // Clear status reg

	Spi & _spi;
	Gpio & _gpio;
	uint32_t _gpio_pin;
};

#endif // !defined(SPIFLASH_HPP)
