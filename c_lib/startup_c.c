#include <inttypes.h>
#include <stddef.h>
#include <picotls.h>

int main(int, char **);
void __libc_init_array(void);

extern char __data_source[];
extern char __data_start[];
extern char __data_end[];
extern char __data_size[];
extern char __bss_start[];
extern char __bss_end[];
extern char __bss_size[];
extern char __tls_base[];
extern char __tdata_end[];
extern char __tls_end[];

void __attribute__((used,section(".init")))
_cstart(void)
{
	for (char * i = &__bss_start[0]; i < &__bss_end[0]; i++)
		*i = 0;
	//_set_tls(__tls_base);
	__libc_init_array();
	(void) main(0, NULL);
	for (;;);
}
