// intellectual property is bullshit bgdc

#include "rsi_bootloader.hpp"
#include "board.hpp"
#include "dbgout.hpp"

#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <inttypes.h>

// TODO don't hard code this
// Enough for 256KB (entire external RAM) + 64KB (header, rounded up to
// erase block size)
#define RAMIMG_LEN (320 * 1024)
#define RAMIMG_ALIGN (64 * 1024)

// There are multiple places you can read write and erase - since the SPI
// flash only has 24-bit addresses, we use the upper 31 bits (ints are signed!)
// to select an address space:
//
// 0x00xxxxxx: FPGA image. The multiboot header will be automatically parsed to
//             find where this should be.
// 0x01xxxxxx: RAM image. The multiboot header will be automatically parsed to
//             find where this should be.
// 0x02xxxxxx: Raw SPI flash with no automatic offsets.
// 0x03xxxxxx: SPI flash JEDEC ID. Read only.

static uint16_t _crc_accumulator;

struct ImageInfo
{
	uint32_t fpga_start;
	uint32_t fpga_len;

	// Address of the RAM image plus its header in the flash
	uint32_t ramimg_start;
	uint32_t ramimg_len;

	// Address for the actual RAM data after the header, used for reading
	// into RAM
	uint32_t ram_start;
	uint32_t ram_len;

	bool valid_header;
	bool valid_fpga;
};

static ImageInfo const * _get_image_info(bool invalidate=false);

// Check that a given address and length fall within a region. If good, returns
// true. Otherwise, sends an error and returns false.
static bool _check_addr(
	uint32_t addr,
	uint32_t n_bytes,
	uint32_t region_start,
	uint32_t region_len
);

// Adjust an address according to its address space. If it doesn't fit the
// region or the space is invalid, send an error and return false.
static bool _check_adjust_addr(
	uint32_t & addr,
	uint32_t n_bytes,
	bool allow_raw
);

static void _spiflash_transfer_cb(uint8_t const * data, size_t n)
{
	for (size_t i = 0; i < n; i++)
		rsi_send_byte(data[i]);
}

static void _spiflash_crc_cb(uint8_t const * data, size_t n)
{
	for (size_t i = 0; i < n; i++)
		_crc_accumulator = rsi_crc16_ccitt_update(
			_crc_accumulator,
			data[i]
		);
}

err_t btl_read(uint8_t const * frame, size_t len, uint8_t cmd)
{
	err_t err;
	int32_t addr;
	uint32_t uaddr;
	uint8_t n_bytes;

	if ((err = rsi_parse_int(&frame, &len, &addr)))
		return err;
	if ((err = rsi_parse_byte(&frame, &len, &n_bytes)))
		return err;

	uaddr = (uint32_t) addr;
	uint8_t addrspace = uaddr >> 24;

	if (addrspace != 0x03)
	{
		if (!_check_adjust_addr(uaddr, n_bytes, true))
			return 0;

		// Bulk read
		uint8_t buf[32];
		rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
		rsi_send_byte(n_bytes);
		SPIFLASH.read_data_chunked(uaddr, buf, sizeof buf, n_bytes,
			_spiflash_transfer_cb);
		rsi_end_packet();
	}
	else
	{
		// Read JEDEC ID
		uint32_t id = SPIFLASH.read_jedec_id();
		uint8_t pkt[3] = {
			(uint8_t)(id >> 16),
			(uint8_t)(id >> 8),
			(uint8_t)(id)
		};
		uaddr &= 0xFFFFFF;

		rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
		rsi_send_byte(n_bytes);
		for (size_t i = uaddr; i != uaddr + n_bytes; i++)
			rsi_send_byte((i < sizeof pkt) ? pkt[i] : 0);
		rsi_end_packet();
	}

	return 0;
}

err_t btl_write(uint8_t const * frame, size_t len, uint8_t cmd)
{
	err_t err;
	int32_t addr;
	uint32_t uaddr;
	uint8_t n_bytes;
	void const * data;

	if ((err = rsi_parse_int(&frame, &len, &addr)))
		return err;
	if ((err = rsi_parse_data(&frame, &len, &n_bytes, &data)))
		return err;

	uaddr = (uint32_t) addr;

	if (!_check_adjust_addr(uaddr, n_bytes, false))
		return 0;

	SPIFLASH.write_page(uaddr, (uint8_t const *) data, n_bytes);

	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_end_packet();

	_get_image_info(true);

	return 0;
}

err_t btl_erase(uint8_t const * frame, size_t len, uint8_t cmd)
{
	// Addr and size must be 64kB aligned
	err_t err;
	int32_t addr, n_bytes;
	uint32_t uaddr, un_bytes;

	if ((err = rsi_parse_int(&frame, &len, &addr)))
		return err;
	if ((err = rsi_parse_int(&frame, &len, &n_bytes)))
		return err;

	uaddr = (uint32_t) addr;
	un_bytes = (uint32_t) n_bytes;

	if (!_check_adjust_addr(uaddr, un_bytes, false))
		return 0;

	if (uaddr & 0xFFFF || un_bytes & 0xFFFF)
		return ERR_ARGVAL;

	while (un_bytes)
	{
		SPIFLASH.erase_block_64KB(uaddr);
		uaddr += 0x10000;
		un_bytes -= 0x10000;
	}

	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_end_packet();

	_get_image_info(true);

	return 0;
}

err_t btl_cksum(uint8_t const * frame, size_t len, uint8_t cmd)
{
	err_t err;
	int32_t addr, n_bytes;
	uint32_t uaddr, un_bytes;

	if ((err = rsi_parse_int(&frame, &len, &addr)))
		return err;
	if ((err = rsi_parse_int(&frame, &len, &n_bytes)))
		return err;

	uaddr = (uint32_t) addr;
	un_bytes = (uint32_t) n_bytes;

	if (!_check_adjust_addr(uaddr, n_bytes, true))
		return 0;

	uint8_t buf[32];
	_crc_accumulator = 0xFFFF;
	SPIFLASH.read_data_chunked(uaddr, buf, sizeof buf, un_bytes,
		_spiflash_crc_cb);

	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_send_uint(_crc_accumulator);
	rsi_end_packet();

	return 0;
}

err_t btl_boot(uint8_t const * frame, size_t len, uint8_t cmd)
{
	// Read from SPI flash.
	// Check if we have a valid image.
	// If not, send back an error; otherwise send ACK.
	// Check if we have a valid RAM partition.
	// If so, read it out into ext RAM.
	// Wait for UART flush
	// Warmboot

	ImageInfo const * info = _get_image_info();
	dbgout("Valid? ", info->valid_fpga);
	if (!info->valid_fpga)
		return ERR_NOBOOT;

	if (info->ram_start)
	{
		dbgout("RS ", info->ram_start);
		dbgout("RS ", info->ram_len);
		SPIFLASH.read_data(
			info->ram_start,
			(uint8_t *) EXT0,
			info->ram_len
		);
	}

	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_end_packet();
	// TODO - wait until UART is flushed!

	dbgout("Boot\n");
	SCTL.warmboot(1);
	return 0;
}

static ImageInfo const * _get_image_info(bool invalidate)
{
	static ImageInfo _info;
	static bool _initialized = false;

	if (invalidate)
	{
		_initialized = false;
		return nullptr;
	}

	if (!_initialized)
	{
		// The ice40 multiboot image is strange. The header consists
		// of five instances of the following:
		// 7E AA 99 7E 92 00 00 44    03 xx xx xx 82 00 00 01
		// 08 00 00 00 00 00 00 00    00 00 00 00 00 00 00 00
		//
		// Each one is located at (0x20 * n), where n=0 points to the
		// power-on reset image and n=1..4 represent boot targets 0..3.
		// The "xx xx xx" is the address where the FPGA bitstream
		// itself can be found. Documentation refers to this as an
		// "applet" rather than a header so maybe it's somehow
		// executable? idfk.
		//
		// The FPGA bitstream starts with something similar:
		// FF 00 00 FF 7E AA 99 7E
		// We check this just to make sure there's a bitstream there.

		union {
			uint8_t buf[32];
			struct {
				uint32_t magic;
				uint32_t len;
			} ramh;
		};
		uint32_t const multiboot_n = 0x1; // We boot from this one
		uint32_t const multiboot_addr = (multiboot_n + 1) * 0x20;

		SPIFLASH.read_data(multiboot_addr, buf, 32);
		_info.valid_header = !memcmp(
			buf,
			"\x7E\xAA\x99\x7E\x92\x00\x00\x44",
			8
		);
		if (_info.valid_header)
		{
			_info.fpga_start  = (uint32_t) buf[11];
			_info.fpga_start |= (uint32_t) buf[10] <<  8;
			_info.fpga_start |= (uint32_t) buf[ 9] << 16;
			// TODO document this assumption...
			_info.fpga_len = _info.fpga_start;

			SPIFLASH.read_data(_info.fpga_start, buf, 8);
			_info.valid_fpga = !memcmp(
				buf,
				"\xFF\x00\x00\xFF\x7E\xAA\x99\x7E",
				8
			);

			// Now read our own RAM init header. It looks like this
			// 52 41 4D 48 xx xx xx 00
			// (R  A  M  H)
			// xxxxxx is the length of the RAM data
			_info.ramimg_start = _info.fpga_start + _info.fpga_len;
			uint32_t rem = _info.ramimg_start % RAMIMG_ALIGN;
			if (rem)
				_info.ramimg_start += (RAMIMG_ALIGN - rem);
			_info.ramimg_len   = RAMIMG_LEN;
			SPIFLASH.read_data(
				_info.ramimg_start,
				(uint8_t *) &ramh,
				sizeof ramh
			);
			if (ramh.magic == 0x484D4152)
			{
				_info.ram_start = _info.ramimg_start + 8;
				_info.ram_len   = ramh.len;
			}
			else
			{
				_info.ram_start = 0;
				_info.ram_len   = 0;
			}
		}
		_initialized = true;
	}

	return &_info;
}

static bool _check_addr(
	uint32_t addr,
	uint32_t n_bytes,
	uint32_t region_start,
	uint32_t region_len
)
{
	if (addr < region_start)
		goto err;
	if (addr >= region_start + region_len)
		goto err;
	if (addr + n_bytes < addr)
		goto err;
	if (addr + n_bytes >= region_start + region_len)
		goto err;
	return true;
err:
	rsi_send_error(ERR_ARGVAL);
	return false;
}

static bool _check_adjust_addr(
	uint32_t & addr,
	uint32_t n_bytes,
	bool allow_raw
)
{
	ImageInfo const * info = _get_image_info();
	uint8_t addrspace = addr >> 24;
	addr &= 0xFFFFFF;

	uint32_t region_start, region_len;

	if (addrspace == 0x00)
	{
		region_start = info->fpga_start;
		region_len   = info->fpga_len;
	}
	else if (addrspace == 0x01)
	{
		region_start = info->ramimg_start;
		region_len   = info->ramimg_len;
	}
	else if (addrspace == 0x02 && allow_raw)
	{
		region_start = 0;
		region_len   = 0x1000000;
	}
	else
		return ERR_ARGVAL;

	addr += region_start;
	return _check_addr(addr, n_bytes, region_start, region_len);
}
