#include "rsi.hpp"

// TODO - this header should be named generically
#include "desc_dualosc_boot.h"
#include "board.hpp"
#include "dbgout.hpp"
#include "rsi_bootloader.hpp"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

typedef err_t (*cmd_func)(uint8_t const * frame, size_t len, uint8_t cmd);
static err_t _gen_ping        (uint8_t const * frame, size_t len, uint8_t cmd);
static err_t _gen_nop         (uint8_t const * frame, size_t len, uint8_t cmd);
static err_t _gen_addr_dir_brd(uint8_t const * frame, size_t len, uint8_t cmd);
static err_t _gen_dsc_read    (uint8_t const * frame, size_t len, uint8_t cmd);

struct cmd_def
{
	uint8_t  grp;
	uint8_t  cmd;
	cmd_func func;
};

cmd_def cmds[] =
{
	{GEN_group, GEN_PING,     _gen_ping},
	{GEN_group, GEN_ADDR_DIR, _gen_addr_dir_brd},
	{GEN_group, GEN_ADDR_BRD, _gen_addr_dir_brd},
	{GEN_group, GEN_DSC_READ, _gen_dsc_read},
	{GEN_group, GEN_ERROR,    _gen_nop},
	{GEN_group, GEN_RESPONSE, _gen_nop},

#if RSI_BOOTLOADER
	{BTL_group, BTL_READ,     btl_read},
	{BTL_group, BTL_WRITE,    btl_write},
	{BTL_group, BTL_ERASE,    btl_erase},
	{BTL_group, BTL_CKSUM,    btl_cksum},
	{BTL_group, BTL_BOOT,     btl_boot},
#endif

	{0, 0, nullptr}
};

static bool _attn(void)
{
	return false; // TODO
}

static int16_t _my_addr_dir = -1, _my_addr_brd = -1;

void rsi_process_packet(uint8_t const * frame, size_t len)
{
	bool attn = _attn();
	bool match = false;
	bool broadcast = false;

	uint8_t addr;
	uint8_t group, cmd;
	bool found_group = false;
	bool found_cmd   = false;
	err_t err;

	if ((err = rsi_parse_byte(&frame, &len, &addr)))
		goto handle_error;

	if (addr == 0xFE)
	{
		match = false;
	}
	else if (addr == _my_addr_brd)
	{
		match = true;
		broadcast = true;
	}
	else if (addr == _my_addr_dir)
	{
		match = true;
	}
	else if (addr == 0x00 && attn)
	{
		match = true;
	}
	else if (addr == 0x01 && attn)
	{
		match = true;
		broadcast = true;
	}
	else if (addr == 0xFF)
	{
		match = true;
		broadcast = true;
	}

	if (!match)
		return;
	if (broadcast)
		rsi_suppress_output();

	if ((err = rsi_parse_byte(&frame, &len, &group)))
		goto handle_error;
	if ((err = rsi_parse_byte(&frame, &len, &cmd)))
		goto handle_error;

	for (size_t i = 0; cmds[i].func; i++)
	{
		if (cmds[i].grp != group)
			continue;
		found_group = true;
		if (cmds[i].cmd != cmd)
			continue;
		found_cmd = true;

		err = cmds[i].func(frame, len, cmd);
		break;
	}

	if (!found_group)
		err = ERR_CGROUP | group;
	else if (!found_cmd)
		err = ERR_CMD | cmd;

	if (!err) return;

handle_error:
	rsi_send_error(err);
}


void rsi_send_error(err_t err)
{
	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_ERROR);
	rsi_send_uint(err);
	rsi_end_packet();
}

err_t rsi_parse_byte(uint8_t const ** frame, size_t * len, uint8_t * val)
{
	if (*len < 1)
		return ERR_ARGLEN;

	*val = *(*frame);
	++(*frame);
	--*len;
	return 0;
}

err_t rsi_parse_int(uint8_t const ** frame, size_t * len, int32_t * val)
{
	if (*len < 4)
		return ERR_ARGLEN;

	uint32_t uval = 0;
	uval = (*frame)[0];
	uval |= (uint32_t)(*frame)[1] << 8;
	uval |= (uint32_t)(*frame)[2] << 16;
	uval |= (uint32_t)(*frame)[3] << 24;

	*val = (int32_t)uval;
	*frame += 4;
	*len -= 4;
	return 0;
}

err_t rsi_parse_data(uint8_t const ** frame, size_t * len, uint8_t * size, void const ** data)
{
	if (*len < 1)
		return ERR_ARGLEN;

	*size = (*frame)[0];
	++*frame;
	--*len;

	if (*len < *size)
		return ERR_ARGLEN;
	*data = (void const *) *frame;
	*frame += *size;
	*len   -= *size;
	return 0;
}

static err_t _gen_ping(uint8_t const * frame, size_t len, uint8_t cmd)
{
	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_end_packet();
	return 0;
}

static err_t _gen_nop(uint8_t const * frame, size_t len, uint8_t cmd)
{
	return 0;
}

static err_t _gen_addr_dir_brd(uint8_t const * frame, size_t len, uint8_t cmd)
{
	int16_t addr;
	uint8_t addr_byte;
	err_t err;
	if ((err = rsi_parse_byte(&frame, &len, &addr_byte)))
	{
		if (err != ERR_ARGLEN)
			return err;
		addr = -1;
	}
	else
	{
		addr = addr_byte;
	}

	if (addr == ADDR_GEO_DIR || addr == ADDR_GEO_BRD ||
			addr == ADDR_REPLY || addr == ADDR_BRD)
	{
		return ERR_ARGVAL | addr;
	}
	if (cmd == GEN_ADDR_DIR)
		_my_addr_dir = addr;
	else
		_my_addr_brd = addr;

	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_end_packet();
	return 0;
}

static err_t _gen_dsc_read    (uint8_t const * frame, size_t len, uint8_t cmd)
{
	int32_t addr;
	uint8_t n_bytes;
	err_t err;

	if ((err = rsi_parse_int(&frame, &len, &addr)))
		return err;
	if ((err = rsi_parse_byte(&frame, &len, &n_bytes)))
		return err;

	size_t uaddr = (size_t) addr;

	rsi_start_packet(ADDR_REPLY, GEN_group, GEN_RESPONSE);
	rsi_send_byte(n_bytes);
	for (size_t i = uaddr; i < uaddr + n_bytes; i++)
	{
		if (i < descriptor_dualosc_boot_len)
			rsi_send_byte(descriptor_dualosc_boot[i]);
		else
			rsi_send_byte(0);
	}
	rsi_end_packet();
	return 0;
}
