// intellectual property is bullshit bgdc

#include "etl/atomic/atomic_gcc_sync.h"
#include "rsi.hpp"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>
#include <etl/queue_spsc_isr.h>

#define CRC_INIT 0xFFFF

static void _send_delim(void);
static uint32_t _send_byte(uint32_t crc, uint8_t b);

#define FRAME_SIZE 256
#define N_FRAMES 2

struct Frame
{
	uint8_t data[FRAME_SIZE];
	uint16_t len;
};

class InterruptControl
{
public:
	static void lock() { rsi_disable_irq(); }
	static void unlock() { rsi_enable_irq(); }
};

static Frame _frames[N_FRAMES];

// queue_spsc_isr is deprecated but gives me smaller code than its replacement.
// fuck it.
static etl::queue_spsc_isr<Frame *, 2, InterruptControl> _frames_empty;
static etl::queue_spsc_isr<Frame *, 2, InterruptControl> _frames_ready;

static Frame * _frame = nullptr;

static uint32_t _out_crc;
static bool _suppressed;

void rsi_init(void)
{
	for (size_t i = 0; i < N_FRAMES; i++)
		_frames_empty.push(&_frames[i]);
}

void rsi_process(void)
{
	Frame * f;
	if (!_frames_ready.pop(f))
		return;

	// TODO this must be done at the right time - ideally right before
	// sending the delimiter. That could get hairy with cases were we don't
	// send anything.
	rsi_enable_rx();

	if (f->len >= 5)
	{
		_suppressed = false;
		rsi_process_packet(f->data, f->len - 2);
	}

	_frames_empty.push(f);
}

bool rsi_handle_byte(uint8_t data)
{
	static bool in_esc = false;
	static uint32_t crc = 0xFFFF;

	if (!_frame)
	{
		if (_frames_empty.pop_from_isr(_frame))
			_frame->len = 0;
	}

	uint16_t const rx_len = _frame ? _frame->len : UINT16_MAX;

	if (data == 0x7D)
	{
		in_esc = true;
		return true;
	}
	else if (data == 0x7E)
	{
		bool more;
		if (rx_len < 2 || rx_len > FRAME_SIZE || !_frame)
		{
			more = true;
		}
		else
		{
			uint16_t rxcrc =
				(uint16_t)(_frame->data[rx_len - 2]) |
				((uint16_t)(_frame->data[rx_len - 1]) << 8);
			if (rxcrc == crc)
			{
				_frames_ready.push_from_isr(_frame);
				_frame = nullptr;
				more = false;
			}
			else
			{
				_frames_empty.push_from_isr(_frame);
				_frame = nullptr;
				more = true;
			}
		}

		crc = 0xFFFF;
		return more;
	}

	if (in_esc)
	{
		data ^= 0x20;
		in_esc = false;
	}

	if (rx_len < FRAME_SIZE)
	{
		_frame->data[rx_len] = data;
		if (rx_len >= 2)
			crc = rsi_crc16_ccitt_update(crc, _frame->data[rx_len - 2]);
	}
	if (rx_len < UINT16_MAX)
		_frame->len = rx_len + 1;
	else if (_frame)
		_frame->len = UINT16_MAX;

	return true;
}

void rsi_start_packet(uint8_t dest, uint8_t cgroup, uint8_t cmd)
{
	_out_crc = CRC_INIT;
	_send_delim();
	_out_crc = _send_byte(_out_crc, dest);
	_out_crc = _send_byte(_out_crc, cgroup);
	_out_crc = _send_byte(_out_crc, cmd);
}

void rsi_send_byte(uint8_t v)
{
	_out_crc = _send_byte(_out_crc, v);
}

void rsi_send_int(int32_t v)
{
	rsi_send_uint(v);
}

void rsi_send_uint(uint32_t v)
{
	for (int i = 0; i < 4; i++)
	{
		_out_crc = _send_byte(_out_crc, v);
		v >>= 8;
	}
}

void rsi_send_data(uint8_t len, uint8_t const * data)
{
	_out_crc = _send_byte(_out_crc, len);
	for (size_t i = 0; i < (size_t) len; i++)
	{
		_out_crc = _send_byte(_out_crc, data[i]);
	}
}

void rsi_send_str(char const * s)
{
	rsi_send_data((uint8_t) strlen(s), (uint8_t const *) s);
}

void rsi_end_packet(void)
{
	_send_byte(0, _out_crc);
	_send_byte(0, _out_crc >> 8);
	_send_delim();
}

void rsi_suppress_output(void)
{
	_suppressed = true;
}

static uint32_t _send_byte(uint32_t crc, uint8_t b)
{
	crc = rsi_crc16_ccitt_update(crc, b);
	if (!_suppressed)
	{
		if (b == 0x7D || b == 0x7E)
		{
			b ^= 0x20;
			rsi_transmit_byte(0x7D);
		}
		rsi_transmit_byte(b);
	}
	return crc;
}

static void _send_delim(void)
{
	if (!_suppressed) rsi_transmit_byte(0x7E);
}
