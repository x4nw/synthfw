// intellectual property is bullshit bgdc

#include "dbgout.hpp"
#include "board.hpp"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#ifdef SCTL_SYSCFG_DBG_bp
static int _dbg_putc(char c)
{
	uint16_t scfg = SCTL.SYSCFG &
		~(SCTL_SYSCFG_DBG_bm | SCTL_SYSCFG_DBG_STROBE);
	SCTL.SYSCFG = scfg;
	SCTL.SYSCFG = scfg
		| ((uint8_t)(c & 0x7F) << SCTL_SYSCFG_DBG_bp)
		| SCTL_SYSCFG_DBG_STROBE;
	return 0;
}
#else
static int _dbg_putc(char c)
{
	(void) c;
	return 0;
}
#endif

static void _hexout(uint8_t b)
{
	_dbg_putc("0123456789ABCDEF"[b >> 4]);
	_dbg_putc("0123456789ABCDEF"[b & 0xF]);
}

void dbgout(char const * label, uint32_t val)
{
	dbgout(label);
	for (int i = 0; i < 4; i++)
	{
		_hexout(val >> 24);
		val <<= 8;
	}
	_dbg_putc('\n');
}

void dbgout(char const * label, void const * val, size_t n)
{
	dbgout(label);
	for (size_t i = 0; i < n; i++)
		_hexout(((uint8_t const *)val)[i]);
	_dbg_putc('\n');
}

void dbgout(char const * label)
{
	char c;
	while ((c = *label++)) _dbg_putc(c);
}
