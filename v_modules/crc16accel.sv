// CRC accelerator connecting to the SERV MDU port. Turns `mul` into `crc16`.
// In theory the other seven MDU instructions could do stuff too... (maybe
// a mode that does two bytes or four bytes at a time would be nice)

module crc16accel
(
	input  wire        i_clk,
	input  wire [31:0] i_ext_rs1,
	input  wire [31:0] i_ext_rs2,
	input  wire [ 2:0] i_ext_funct3,
	output reg  [31:0] o_ext_rd,
	output wire        o_ext_ready,
	input  wire        i_mdu_valid
);

logic valid;
logic [15:0] crc_out;
logic [15:0] crc_latched;
always_ff @(posedge i_clk) valid <= i_mdu_valid;
always_ff @(posedge i_clk) if (i_mdu_valid) crc_latched <= crc_out;
assign o_ext_ready = valid & i_mdu_valid;
assign o_ext_rd    = {16'h0, crc_latched};

crc16 crc_calc
(
	.crcIn (i_ext_rs2[15:0]),
	.data  (i_ext_rs1[ 7:0]),
	.crcOut(crc_out)
);

endmodule
