// WISHBONE DATASHEET
// ------------------
// General description:    32-bit to 8-bit wishbone converter
// Supported cycles:       DEVICE, READ/WRITE
//                         DEVICE, BLOCK READ/WRITE
//                         DEVICE, RMW
//                         HOST, READ/WRITE
//                         HOST, BLOCK READ/WRITE
//                         HOST, RMW
// Data port, size:        32-bit
// Data port, granularity: 8-bit
//
// Accesses sent to bus_resizer's 32-bit port are narrowed and passed to its
// 8-bit port as multiple bus transactions. Note that the low two bits of the
// 32-bit upstream-facing port are ignored, so the upstream bus must always
// be aligned.

module bus_resizer
#(
	parameter ADDR_WIDTH = 32
)
(
	input wire i_clk,
	input wire i_rst,

	// -- 32-bit bus --
	input  wire [ADDR_WIDTH-1:0] i_32b_adr,
	input  wire [          31:0] i_32b_dat,
	input  wire [           3:0] i_32b_sel,
	input  wire                  i_32b_we,
	input  wire                  i_32b_cyc,
	input  wire                  i_32b_stb,
	output wire [          31:0] o_32b_dat,
	output wire                  o_32b_ack,

	// -- 8-bit bus --
	output wire [ADDR_WIDTH-1:0] o_8b_adr,
	output reg  [           7:0] o_8b_dat,
	output wire                  o_8b_we,
	output wire                  o_8b_cyc,
	output wire                  o_8b_stb,
	input  wire [           7:0] i_8b_dat,
	input  wire                  i_8b_ack
);

localparam S_RW0  = 3'b100;
localparam S_RW1  = 3'b101;
localparam S_RW2  = 3'b110;
localparam S_RW3  = 3'b111;
localparam S_IDLE = 3'b000;

reg  [2:0]  state = S_IDLE;
reg  [23:0] data  = 24'h0;

wire [1:0] bytesel = state[1:0];
wire       run     = state[2];

// Output data mux
always_comb case (bytesel)
	2'h0: o_8b_dat = i_32b_dat[ 7: 0];
	2'h1: o_8b_dat = i_32b_dat[15: 8];
	2'h2: o_8b_dat = i_32b_dat[23:16];
	2'h3: o_8b_dat = i_32b_dat[31:24];
endcase

// Input data latches
always_ff @(posedge i_clk) begin
	if (bytesel == 2'h0) data[ 7: 0] <= i_8b_dat;
	if (bytesel == 2'h1) data[15: 8] <= i_8b_dat;
	if (bytesel == 2'h2) data[23:16] <= i_8b_dat;
end
assign o_32b_dat = {i_8b_dat, data};

// Byte selects
wire sel = i_32b_sel[bytesel];
wire proceed = i_8b_ack || !sel;

// Output control signals
assign o_8b_adr  = {i_32b_adr[ADDR_WIDTH-1:2], bytesel};
assign o_8b_we   = i_32b_we;
assign o_8b_stb  = run && sel;
assign o_8b_cyc  = i_32b_cyc;
assign o_32b_ack = i_32b_stb && proceed && (state == S_RW3);

reg [2:0] next_state;

always_comb begin
	next_state = state;
	case (state)
		S_IDLE: if (i_32b_stb) next_state = S_RW0;
		S_RW0: if (proceed) next_state = S_RW1;
		S_RW1: if (proceed) next_state = S_RW2;
		S_RW2: if (proceed) next_state = S_RW3;
		S_RW3: if (proceed) next_state = S_IDLE;
		default: next_state = S_IDLE;
	endcase
end

always_ff @(posedge i_clk)
	if (i_rst)
		state <= S_IDLE;
	else
		state <= next_state;

endmodule
