// WISHBONE DATASHEET
// ------------------
// General description:    System controller (interrupts, timer, reset, reboot)
// Supported cycles:       DEVICE, READ/WRITE
//                         DEVICE, BLOCK READ/WRITE
//                         DEVICE, RMW
// Data port, size:        32-bit
// Data port, granularity: 8-bit, some registers 32-bit
//
// REGISTER MAP
// ------------
// Ad  Sz  R/W  Name
// 00  16  R/W  INTEN    Interrupt enable bits (1 = enabled)
// 02  16  R/W  SYSCFG   System configuration register
// 04  16  R/W  INTFLAGS Interrupt flag bits (1 = active)
// 06  16  ---  ---      reserved (always reads 0)
// 08  32  R/W  DIVISOR  Timer divisor
// 0C  32    W  WARMBOOT Warmboot trigger
//
// INTEN:    A 1 in each position enables the corresponding interrupt source.
//           Bit 15 is reserved for the internal timer interrupt.
//           8-bit granularity.
// SYSCFG:   Directly forwarded to o_syscfg[]. Meant for use by the SOC for
//           arbitrary configuration features.
//           8-bit granularity.
// INTFLAGS: A 1 in each position indicates that interrupt is active. Bit 15
//           is reserved for the internal timer interrupt. Writing a 1 to bit
//           15 acks/clears the timer interrupt; writing to other bits has no
//           effect.
//           8-bit granularity.
// DIVISOR:  Sets the timer divisor. Timer frequency is f(i_clk)/(DIVISOR + 1)
//           32-bit granularity (smaller writes are rejected)
// WARMBOOT: Triggers WARMBOOT[1:0] to be written to o_warmboot_sel, then
//           1 to be written to o_warmboot_boot one cycle later, when
//           WARMBOOT[31:2] is written with KEY_WARMBOOT (30'h5B985D1, see
//           below).
//           32-bit granularity (smaller writes are rejected)
//
// TIMER
// -----
// The timer can be used to generate periodic interrupts. Write a divisor into
// the DIVISOR register, then enable the interrupt via INTFLAGS[15]. Writing
// the divisor will reset the timer and trigger an interrupt immediately. To
// single-shot trigger an interrupt, write INTEN[15] to zero, write the
// divisor, write INTFLAGS[15] to one to dismiss the initial interrupt, then
// write INTFLAGS[15] to one.
//
// The timer does not stop counting when an interrupt is generated. To
// acknowledge the interrupt, write a 1 to INTFLAGS[15].
//
// INTERRUPT CONTROLLER
// --------------------
// The interrupt controller combines up to 16 interrupt sources, including the
// internal timer, into a single-priority interrupt output. An interrupt is
// enabled if its INTEN bit is set. Interrupts are level sensitive and must be
// acknowled at their originating sources.
//
// RESET GENERATOR
// ---------------
// A reset generator will begin to count down when i_rstreq is asserted, for
// at least RESET_COUNT counts, and o_rst will assert during this interval.
// i_rstreq must be synchronous to i_clk. o_rst is a vector of N_RESETS
// identical signals, reducing synchronous reset fanout and improving timing
// closure. You should experiment and find the number of resets that produces
// the best results for your design (up to one per load).
//
// The rest of the logic in sctl is reset via i_rst, which generally should be
// supplied from one of the o_rst outputs.
//
// WARMBOOT CONTROLLER
// -------------------
// The warmboot controller generates signals suitable for driving ice40's
// SB_WASRMBOOT block. To prevent accidental reboots, a key must be written
// to the trigger register. Writes of any other value do not trigger output.
//
// SYSCFG
// ------
// SYSCFG is a 16-bit register directly passed to the output port. It is
// suggested for use as runtime configuration bits internal to the SOC.

module sctl
#(
	parameter RESET_COUNT = 32,
	parameter N_RESETS = 2
)
(
	input  logic        i_clk,
	input  logic        i_rst,
	input  logic [ 3:0] i_adr,
	input  logic [31:0] i_dat,
	input  logic [ 3:0] i_sel,
	output logic [31:0] o_dat,
	input  logic        i_we,
	input  logic        i_stb,
	output logic        o_ack,
	output logic        o_int, // Interrupt to CPU

	// Interrupt request inputs and system config
	input  logic [14:0] i_irq,
	output logic [15:0] o_syscfg,

	// Warmboot control outputs
	output logic        o_warmboot_boot,
	output logic [ 1:0] o_warmboot_sel,

	// Reset controller. Generally, you should loop o_rst back to i_rst
	input  logic        i_rstreq,
	output logic [N_RESETS-1:0] o_rst
);

localparam ADDR_INTEN_SYSCFG = 2'h0;
localparam ADDR_INTFLAGS     = 2'h1;
localparam ADDR_DIVISOR      = 2'h2;
localparam ADDR_WARMBOOT     = 2'h3;
localparam KEY_WARMBOOT      = 30'h5B985D1;

// Shortcuts for common functions
wire         wr               = i_stb && i_we && !o_ack;
wire  [ 1:0] wadr             = i_adr[3:2];

// Interrupt control and syscfg
logic [15:0] syscfg           = 16'h0;
logic [15:0] inten            = 16'h0;
assign o_syscfg = syscfg;

// Timer
logic [31:0] divisor          = 32'h0;
logic [31:0] timer            = 32'h0;
logic        timer_reset      =  1'b0;
logic        timer_ack        =  1'b0;
logic        timer_irq        =  1'b0;

// Warmboot
logic        warmboot_trigger =  1'b0;
logic        warmboot_boot    =  1'b0;
logic [ 1:0] warmboot_sel     =  2'b0;

// Reset generator
// verilator lint_off WIDTHTRUNC
wire  [$clog2(RESET_COUNT)-1:0] reset_top  = RESET_COUNT - 1;
// verilator lint_on WIDTHTRUNC
logic [$clog2(RESET_COUNT)-1:0] reset_ctdn = reset_top;

assign o_warmboot_sel  = warmboot_sel;
assign o_warmboot_boot = warmboot_boot;

// Reset generator
logic rst = 1'b1;
always_ff @(posedge i_clk) begin
	if (i_rstreq) begin
		reset_ctdn <= reset_top;
		rst <= 1'b1;
	end else if (reset_ctdn != 0) begin
		reset_ctdn <= reset_ctdn - 1;
	end else begin
		rst <= 1'b0;
	end
end

`ifdef VERILATOR
always_ff @(posedge i_clk) begin
	o_rst <= {N_RESETS{rst}};
end
`else
for (genvar i = 0; i < N_RESETS; i++)
	SB_DFF rst_dff (.Q(o_rst[i]), .C(i_clk), .D(rst));
`endif

// Delay the warmboot trigger so the select bits arrive first
always_ff @(posedge i_clk) begin
	warmboot_boot <= i_rst ? 1'b0 : warmboot_trigger;
end

// Compute the output interrupt state
always_ff @(posedge i_clk) begin
	o_int <= |(inten & {timer_irq, i_irq});
end

// Timer
always_ff @(posedge i_clk) begin
	if (i_rst)
		timer <= 0;
	else if (timer_reset)
		timer <= 0;
	else if (timer == divisor)
		timer <= 0;
	else
		timer <= timer + 1;
end

// Timer interrupt generator
always_ff @(posedge i_clk) begin
	if (i_rst) begin
		timer_irq <= 1'b0;
	end else if (timer_ack || timer_reset) begin
		timer_irq <= 1'b0;
	end else if (timer == 0) begin
		timer_irq <= 1'b1;
	end
end

// Bus control interface
always_ff @(posedge i_clk) o_ack <= i_stb;

// Registers
always_ff @(posedge i_clk) begin
	if (!wr)
		timer_ack <= 1'b0;
	else if (wr && wadr == ADDR_INTFLAGS && i_sel[1] && i_dat[15])
		timer_ack <= 1'b1;
end

always_ff @(posedge i_clk) begin
	if (!wr)
		timer_reset <= 1'b0;
	else if (wr && wadr == ADDR_DIVISOR)
		timer_reset <= 1'b1;
end

always_ff @(posedge i_clk) begin
	if (i_rst) begin
		inten <= 16'h0;
		syscfg <= 16'h0;
	end else if (wr && wadr == ADDR_INTEN_SYSCFG) begin
		if (i_sel[3]) syscfg[15:8] <= i_dat[31:24];
		if (i_sel[2]) syscfg[ 7:0] <= i_dat[23:16];
		if (i_sel[1]) inten [15:8] <= i_dat[15: 8];
		if (i_sel[0]) inten [ 7:0] <= i_dat[ 7: 0];
	end
end

always_ff @(posedge i_clk) begin
	if (i_rst)
		divisor <= 32'h0;
	else if (wr && wadr == ADDR_DIVISOR && i_sel == 4'hF)
		divisor <= i_dat;
end

always_ff @(posedge i_clk) begin
	if (i_rst) begin
		warmboot_trigger <= 1'b0;
		warmboot_sel     <= 2'b0;
	end else if (
		wr &&
		wadr == ADDR_WARMBOOT &&
		i_sel == 4'hF &&
		i_dat[31:2] == KEY_WARMBOOT
	) begin
		warmboot_trigger <= 1'b1;
		warmboot_sel     <= i_dat[1:0];
	end
end

// Memory readback
always_comb begin
	case (wadr)
		ADDR_INTEN_SYSCFG: o_dat = {syscfg, inten};
		ADDR_INTFLAGS:     o_dat = {16'h0, timer_irq, i_irq};
		ADDR_DIVISOR:      o_dat = divisor;
		ADDR_WARMBOOT:     o_dat = 32'h0;
	endcase
end

endmodule
