// Module wrapping SERV with a conversion to a more proper Wishbone interface

module wbserv #(
	parameter ADDR_WIDTH = 32,
	parameter CRCACCEL = 0
)
(
	input  logic        i_clk,
	input  logic        i_rst,
	output logic [ADDR_WIDTH-1:0] o_adr,
	output logic [31:0] o_dat,
	output logic [ 3:0] o_sel,
	input  logic [31:0] i_dat,
	output logic        o_we,
	output logic        o_cyc,
	input  logic        i_ack,
	input  logic        i_int
);

// SERV instruction bus
logic [31:0] ibus_adr;
logic        ibus_cyc;
logic [31:0] ibus_rdt;
logic        ibus_ack;

// SERV data bus
logic [31:0] dbus_adr;
logic [31:0] dbus_dat;
logic [3:0]  dbus_sel;
logic        dbus_we;
logic        dbus_cyc;
logic [31:0] dbus_rdt;
logic        dbus_ack;

// MDU bus
logic [31:0] ext_rs1;
logic [31:0] ext_rs2;
logic [ 2:0] ext_funct3;
logic [31:0] ext_rd;
logic        ext_ready;
logic        mdu_valid;

serv_rf_top #(
	.MDU(CRCACCEL),
	.RESET_STRATEGY("NONE"),
	.PRE_REGISTER(0) // doc says this is larger, but i actually find it smaller
)
serv (
	.clk        (i_clk),
	.i_rst      (i_rst),
	.i_timer_irq(i_int),

	.o_ibus_adr (ibus_adr),
	.o_ibus_cyc (ibus_cyc),
	.i_ibus_rdt (ibus_rdt),
	.i_ibus_ack (ibus_ack),

	.o_dbus_adr (dbus_adr),
	.o_dbus_dat (dbus_dat),
	.o_dbus_sel (dbus_sel),
	.o_dbus_we  (dbus_we),
	.o_dbus_cyc (dbus_cyc),
	.i_dbus_rdt (dbus_rdt),
	.i_dbus_ack (dbus_ack),

	.o_ext_rs1  (ext_rs1),
	.o_ext_rs2  (ext_rs2),
	.o_ext_funct3(ext_funct3),
	.i_ext_rd   (ext_rd),
	.i_ext_ready(ext_ready),
	.o_mdu_valid(mdu_valid)
);

generate
if (CRCACCEL) begin
	crc16accel accel (
		.i_clk       (i_clk),
		.i_ext_rs1   (ext_rs1),
		.i_ext_rs2   (ext_rs2),
		.i_ext_funct3(ext_funct3),
		.o_ext_rd    (ext_rd),
		.o_ext_ready (ext_ready),
		.i_mdu_valid (mdu_valid)
	);
end else begin
	assign ext_rd = 32'h0;
	assign ext_ready = 1'b1;
end
endgenerate

logic dirty_ibus_ack, dirty_dbus_ack;
logic [31:0] full_wb_adr;
servile_arbiter
cpu_arb (
	.i_wb_cpu_dbus_adr(dbus_adr),
	.i_wb_cpu_dbus_dat(dbus_dat),
	.i_wb_cpu_dbus_sel(dbus_sel),
	.i_wb_cpu_dbus_we (dbus_we),
	.i_wb_cpu_dbus_stb(dbus_cyc),
	.o_wb_cpu_dbus_rdt(dbus_rdt),
	.o_wb_cpu_dbus_ack(dirty_dbus_ack),

	.i_wb_cpu_ibus_adr(ibus_adr),
	.i_wb_cpu_ibus_stb(ibus_cyc),
	.o_wb_cpu_ibus_rdt(ibus_rdt),
	.o_wb_cpu_ibus_ack(dirty_ibus_ack),

	.o_wb_mem_adr     (full_wb_adr),
	.o_wb_mem_dat     (o_dat),
	.o_wb_mem_sel     (), // <-- broken, generate below :/
	.o_wb_mem_we      (o_we),
	.o_wb_mem_stb     (o_cyc),
	.i_wb_mem_rdt     (i_dat),
	.i_wb_mem_ack     (i_ack)
);
// lol what is it with serv and dirty wishbone
// it's not hard to clean this up
assign o_sel    = ibus_cyc ? 4'hF : dbus_sel;
assign dbus_ack = dbus_cyc && dirty_dbus_ack;
assign ibus_ack = ibus_cyc && dirty_ibus_ack;
assign o_adr    = full_wb_adr[ADDR_WIDTH-1:0];

endmodule
