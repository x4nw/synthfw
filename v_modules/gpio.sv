// 32-bit GPIO module, styled after the newer AVR GPIOs

// WISHBONE DATASHEET
// ------------------
// General description:    32-bit GPIO port
// Supported cycles:       DEVICE, READ/WRITE
//                         DEVICE, BLOCK READ/WRITE
//                         DEVICE, RMW
// Data port, size:        32-bit
// Data port, granularity: 32-bit (partial writes are ignored)
//
// REGISTER MAP
// ------------
// 00  R/W  DIR       1 = direction=output, 0 = direction=input
// 04    W  DIRSET    1 = atomically set to output
// 08    W  DIRCLR    1 = atomically set to input
// 0C    W  DIRTGL    1 = atomically toggle direction
// 10  R/W  OUT       1 = output level is 1, 0 = output level is 0
// 04    W  OUTSET    1 = atomically set output to 1
// 08    W  OUTCLR    1 = atomically set output to 0
// 0C    W  OUTTGL    1 = atomically toggle output
// 20  R    IN        1 = pin input value is 1
// 24  R/_  INTFLAGS  1 = interrupt is currently asserted on pin
//     _/W            1 = atomically acknowledge/clear interrupt on pin
// 28  R/W  INTEN_R   1 = rising-edge interrupt is enabled on pin
// 2C  R/W  INTEN_F   1 = falling-edge interrupt is enabled on pin
// 30  R/W  AUX       1 = auxiliary bypass is enabled on pin
//
// REGISTER MAP NOTES
// ------------------
// 1. DIRSET/CLR/TGL registers may be read, but they will return the value of
//    the DIR register.
// 2. OUTSET/CLR/TGL registers may be read, but they will retunr the value of
//    the OUT register.
// 3. AUX always reads back zero for pins not having HAS_AUX set.
// 4. "Invalid" registers always safely read zero and do not take writes. No
//    error output is provided, but incorrect accesses are not dangerous.
// 5. Writes to less than 32 bits at a time are ignored.
// 6. adr_i[1:0] are ignored. Only use on an aligned bus.
//
// INTERRUPTS
// ----------
// Interrupts are always edge-triggered. When an edge is detected and the
// interrupt is enabled for that pin and edge type, o_int will be asserted
// until the interrupt is acknowledged by writing a 1 to the corresponding
// bit in INTFLAGS.
//
// If an edge is detected simultaneous to an acknowledgment via INTFLAGS, the
// interrupt remains asserted.
//
// AUXILIARY FUNCTION - COMBINATIONAL LOGIC WARNING
// ------------------------------------------------
// When HAS_AUX[pin] is set and the AUX[pin] register bit is set, the GPIO
// output is taken directly from i_aux[pin] instead of from the internal
// logic. This allows another periperal to optionally drive the same pin.
// BEWARE: this inserts a LUT *AFTER* the output register latch. You MUST
// register the pin output itself in this case, delaying it by one more cycle,
// to avoid glitches. The path delay is short, so you may be able to use a
// registered DDR output to reduce the delay to a half-cycle.
//
// If HAS_AUX[pin] is zero, o_out[] is safely driven directly from flipflops.
//
// o_aux is derived directly from i_in and is used to route aux inputs to
// peripherals. It is not disconnected when AUX[pin] is clear.
//
// If AUX_OPENDRAIN[pin] and HAS_AUX[pin] are both set, then when AUX[pin] is
// set, o_dir[pin] is additionally driven to the inverse of i_aux[pin]. This
// allows open-drain peripherals such as I2C to be switched in. In this case,
// o_dir[pin] should also be registered.

module gpio
#(
	// If the inputs are already synchronized, you can save some LUTs by
	// making this zero.
	parameter [0:0] SYNCHRONIZE = 1,

	// If a bit in HAS_AUX is 1, that pin supports the auxiliary function.
	parameter [31:0] HAS_AUX = 32'h00000000,

	// If a bit in AUX_OPENDRAIN is 1 and the pin supports the auxiliary
	// function, when placed in aux mode the pin is driven open-drain:
	// i_aux[pin] == 1   gives   o_out[pin] = 1 and o_dir[pin] = 0
	// i_aux[pin] == 0   gives   o_out[pin] = 0 and o_dir[pin] = 1
	parameter [31:0] AUX_OPENDRAIN = 32'h00000000
)
(
	input  logic        i_clk,
	input  logic        i_rst,
	input  logic [ 5:0] i_adr,
	input  logic [31:0] i_dat,
	input  logic [ 3:0] i_sel,
	output logic [31:0] o_dat,
	input  logic        i_we,
	input  logic        i_stb,
	output logic        o_ack,
	output logic        o_int,

	// Auxiliary IO
	input  logic [31:0] i_aux,
	output logic [31:0] o_aux,

	// Output to GPIO drivers
	output logic [31:0] o_out,
	// Direction control to GPIO drivers
	output logic [31:0] o_dir,
	// Input from GPIO receivers
	input  logic [31:0] i_in
);

localparam ADRR_DIR      = 4'b00??;
localparam ADRW_DIR      = 4'b0000;
localparam ADRW_DIRSET   = 4'b0001; // reads DIR
localparam ADRW_DIRCLR   = 4'b0010; // reads DIR
localparam ADRW_DIRTGL   = 4'b0011; // reads DIR

localparam ADRR_OUT      = 4'b01??;
localparam ADRW_OUT      = 4'b0100;
localparam ADRW_OUTSET   = 4'b0101; // reads OUT
localparam ADRW_OUTCLR   = 4'b0110; // reads OUT
localparam ADRW_OUTTGL   = 4'b0111; // reads OUT

localparam ADRR_IN       = 4'b1000;
localparam ADRW_IN       = 4'b1000;

localparam ADRR_INTFLAGS = 4'b1001;
localparam ADRW_INTFLAGS = 4'b1001;

localparam ADRR_INTEN_R  = 4'b1010;
localparam ADRW_INTEN_R  = 4'b1010;

localparam ADRR_INTEN_F  = 4'b1011;
localparam ADRW_INTEN_F  = 4'b1011;

localparam ADRR_AUX      = 4'b1100;
localparam ADRW_AUX      = 4'b1100;

logic [31:0] dir      = 32'h0;
logic [31:0] out      = 32'h0;
logic [31:0] in       = 32'h0;
logic [31:0] intflags = 32'h0;
logic [31:0] inten_r  = 32'h0;
logic [31:0] inten_f  = 32'h0;
logic [31:0] aux      = 32'h0;

wire  [31:0] in_prev;
logic [31:0] rise;
logic [31:0] fall;

wire   wr = i_stb &&  i_we && i_sel == 4'hF;
wire   rd = i_stb && !i_we;
wire  [3:0] adr = i_adr[5:2];
assign o_ack = i_stb;

// IO drivers
for (genvar gi = 0; gi < 32; gi++) begin
	if (HAS_AUX[gi] && AUX_OPENDRAIN[gi])
		assign o_dir[gi] = aux[gi] ? ~i_aux[gi] : dir[gi];
	else
		assign o_dir[gi] = dir[gi];
end

for (genvar gi = 0; gi < 32; gi++) begin
	if (HAS_AUX[gi])
		assign o_out[gi] = aux[gi] ? i_aux[gi] : out[gi];
	else
		assign o_out[gi] = out[gi];
end

assign o_aux = i_in;

// Input synchronizers
generate
if (SYNCHRONIZE) begin
	logic [31:0] in_sync1   = 32'h0;
	logic [31:0] in_sync2   = 32'h0;
	assign in_prev = in_sync2;
	always_ff @(posedge i_clk) begin
		in_sync1 <= i_in;
		in_sync2 <= in_sync1;
		in       <= in_sync2;
	end
end else begin
	assign in_prev = i_in;
	always_ff @(posedge i_clk) begin
		in <= in_prev;
	end
end
endgenerate

// Interrupt detectors
always_ff @(posedge i_clk) begin
	rise <= ~in & in_prev;
	fall <= in & ~in_prev;
	o_int <= |intflags;
end

// Interrupts
for (genvar gi = 0; gi < 32; gi++)
always_ff @(posedge i_clk) begin
	if (i_rst) begin
		intflags[gi] <= 1'b0;
	end else if (rise[gi] && inten_r[gi]) begin
		intflags[gi] <= 1'b1;
	end else if (fall[gi] && inten_f[gi]) begin
		intflags[gi] <= 1'b1;
	end else if (wr && (adr == ADRW_INTFLAGS) && i_dat[gi]) begin
		intflags[gi] <= 1'b0;
	end
end

// Outputs
always_ff @(posedge i_clk) begin
	if (i_rst) begin
		dir     <= 32'h0;
		out     <= 32'h0;
		inten_r <= 32'h0;
		inten_f <= 32'h0;
		aux     <= 32'h0;
	end else if (wr) case (adr)
		ADRW_DIR:     dir <= i_dat;
		ADRW_DIRSET:  dir <= dir |  i_dat;
		ADRW_DIRCLR:  dir <= dir & ~i_dat;
		ADRW_DIRTGL:  dir <= dir ^  i_dat;
		ADRW_OUT:     out <= i_dat;
		ADRW_OUTSET:  out <= out |  i_dat;
		ADRW_OUTCLR:  out <= out & ~i_dat;
		ADRW_OUTTGL:  out <= out ^  i_dat;
		ADRW_INTEN_R: inten_r <= i_dat;
		ADRW_INTEN_F: inten_f <= i_dat;
		ADRW_AUX:     aux <= i_dat;
		default: ;
	endcase
end

// Inputs
reg [31:0] rdat;
always_ff @(posedge i_clk) begin
	casez (adr)
		ADRR_DIR:      rdat = dir;
		ADRR_OUT:      rdat = out;
		ADRR_IN:       rdat = in;
		ADRR_INTFLAGS: rdat = intflags;
		ADRR_INTEN_R:  rdat = inten_r;
		ADRR_INTEN_F:  rdat = inten_f;
		ADRR_AUX:      rdat = aux & HAS_AUX;
		default:       rdat = 32'h0;
	endcase
	if (rd) o_dat <= rdat;
end

endmodule
