module icegpio #(
	// Enable pullup
	parameter [0:0] PULLUP = 0,
	// Registered output allows the pin to be driven from combinational
	// logic
	parameter [0:0] REGO = 1,
	// Faster registered output: use the DDR output
	parameter [0:0] FASTREGO = 0
)(
	input  logic clk,
	output logic in,
	input  logic out,
	input  logic dir,
	inout  wire  pad
);

`include "ice40_pintypes.v"

localparam OUTMODE =
	{FASTREGO, REGO} == 2'b00 ? PIN_OUTPUT_TRISTATE :
	{FASTREGO, REGO} == 2'b01 ? PIN_OUTPUT_REG_EN_REG :
	{FASTREGO, REGO} == 2'b10 ? PIN_OUTPUT_TRISTATE :
	                            PIN_OUTPUT_DDR_EN_REG;

SB_IO #(
	.PIN_TYPE({PIN_INPUT_REG, OUTMODE}),
	.PULLUP(PULLUP ? 1'b1 : 1'b0)
) iob (
	.PACKAGE_PIN(pad),
	.INPUT_CLK(clk),
	.OUTPUT_CLK(clk),
	.OUTPUT_ENABLE(dir),
	.D_OUT_0(out),
	.D_OUT_1(out),
	.D_IN_0(in)
);

endmodule
