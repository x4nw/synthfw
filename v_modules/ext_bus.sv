// WISHBONE DATASHEET
// ------------------
// General description:    8-bit external bus interface
// Supported cycles:       DEVICE, READ/WRITE
//                         DEVICE, BLOCK READ/WRITE
//                         DEVICE, RMW
// Data port, size:        8-bit
// Data port, granularity: N/A
//
// Adapts an 8-bit Wishbone bus to a classic SRAM-style external bus interface,
// with parameter-specified timings. Note that the address bus is not passed
// through this module; it should be sent directly to the external interface.

module ext_bus #(
	// Number of cycles from asserting addr/nrd/stb to latching data
	// (max(tAA, tDOE) in datasheet)
	parameter ADDR_TO_RD_READY = 1,
	// Number of cycles from latching data to deasserting addr/nrd/stb
	parameter RD_HOLD = 1,
	// Number of cycles from asserting addr/data/nwe to asserting stb
	// (tSA)
	parameter WR_SETUP = 1,
	// Number of cycles to hold stb
	// (tSCS1)
	parameter WR_STROBE = 1,
	// Number of cycles from asserting addr/data/nwe to deasserting
	// addr/data
	// (tWC - tSCS1)
	parameter WR_HOLD = 1
)
(
	input  wire        i_clk,
	input  wire        i_rst,
	input  wire        i_stb,
	input  wire        i_we,
	input  wire [ 7:0] i_dat,
	output reg  [ 7:0] o_dat,
	output reg         o_ack,

	output wire [ 7:0] o_ebi_d,
	input  wire [ 7:0] i_ebi_d,
	output wire        o_ebi_doe,
	output wire        o_ebi_nrd,
	output wire        o_ebi_nwr,
	output reg         o_ebi_cs
);

localparam rd_top = ADDR_TO_RD_READY + RD_HOLD + 2;
localparam wr_top = WR_HOLD + WR_STROBE + WR_SETUP + 2;
localparam max_count = rd_top > wr_top ? rd_top : wr_top;
localparam wcount = $clog2(max_count + 1);

// Read cycle:
//                |<- RD_HOLD + 2
//                |   |<- 2
// i_stb  ___/^^^^^^^^^^\_
// o_ack  ____________/^\_
// count     L    .   .
// o_cs   ___/^^^^\______
// o_dat  --------<=====>

// Write cycle:
//                |<- WR_STROBE + WR_HOLD + 2
//                |   |<- WR_HOLD + 2
//                |   |   |<- 2
// i_stb  ___/^^^^^^^^^^^^^^\_
// o_ack  ________________/^\_
// count     L    .   .   .
// o_cs   ________/^^^\_______
// ebi_d  ---<============>---

reg [wcount-1:0] count = 0;
reg data_oe = 1'b0;

assign o_ebi_d  = i_dat;
assign o_ebi_doe = data_oe;
assign o_ebi_nrd = i_we;
assign o_ebi_nwr = !i_we;

always_ff @(posedge i_clk) begin
	if (i_rst) count <= 0;
	else if (count > 0) count <= count - 1;
	else if (i_stb) count <= i_we ? wr_top : rd_top;
end

always_ff @(posedge i_clk) begin
	if (i_rst || (!i_stb && count == 0)) begin
		data_oe <= 1'b0;
		o_ebi_cs <= 1'b0;
		o_ack <= 1'b0;
	end if (i_we) begin
		if (count == wr_top) begin
			data_oe <= 1'b1;
		end else if (count == WR_STROBE + WR_HOLD + 2) begin
			o_ebi_cs <= 1'b1;
		end else if (count == WR_HOLD + 2) begin
			o_ebi_cs <= 1'b0;
		end else if (count == 2) begin
			o_ack <= 1'b1;
		end else if (count == 1) begin
			o_ack <= 1'b0;
		end
	end else begin
		if (count == rd_top) begin
			o_ebi_cs <= 1'b1;
		end else if (count == RD_HOLD + 2) begin
			o_dat <= i_ebi_d;
			o_ebi_cs <= 1'b0;
		end else if (count == 2) begin
			o_ack <= 1'b1;
		end else if (count == 1) begin
			o_ack <= 1'b0;
		end
	end
end

endmodule
