// This file provides a DPI-compatible adapter to qspiflashsim.cpp, allowing me
// to invert the usual structure (qspiflashsim is meant to be called from a
// verilator C++ testbench, but i'm using a SV testbench).

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <vector>
#include <memory>
#include <stdexcept>
#include <stdio.h>

#include "../thirdparty/qspiflashsim/qspiflashsim.h"

#define TOO_MANY_FLASHES 1024
static std::vector<std::unique_ptr<QSPIFLASHSIM>> _flashes;

// Init the nth flash with address length addr_len
extern "C"
void qspiflashsim_init(unsigned n, int addr_len, bool debug)
{
	if (n > TOO_MANY_FLASHES)
		throw std::runtime_error("sorry, that's too many flashes");

	while (_flashes.size() <= n)
		_flashes.push_back(nullptr);

	fprintf(stderr, "Initializing flash %u, addr_len %u%s\n",
			n, addr_len, debug ? ", debug mode" : "");
	_flashes[n] = std::make_unique<QSPIFLASHSIM>(addr_len, debug);
}

// Load the nth flash from a binary file
extern "C"
void qspiflashsim_load(unsigned n, unsigned addr, const char * fname)
{
	if (n >= _flashes.size() || !_flashes[n])
		throw std::runtime_error("this flash doesn't exist");

	fprintf(stderr, "Loading flash %u at 0x%X with %s\n",
			n, addr, fname);
	_flashes[n]->load(addr, fname);
}

// Execute the flash, returning data_out
extern "C"
int qspiflashsim_run(unsigned n, int csn, int sck, int dat)
{
	if (n >= _flashes.size() || !_flashes[n])
		throw std::runtime_error("this flash doesn't exist");

	return (*_flashes[n])(csn, sck, dat);
}
