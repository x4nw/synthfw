#!/bin/bash
#
# Quick tool to report embedded firmware size and percentage of memory used.
# Assumes a single address space.
#
# size.sh SIZE_TOOL ELF_FILE TOTAL_CAPACITY [FILE]
#
# [FILE] is an optional file to create. This lets size.sh be used as a fake
# build target and let the build system know it has completed successfully.

SIZE_TOOL="$1"
ELF_FILE="$2"
TOTAL_CAPACITY="$3"

size_used=$("$SIZE_TOOL" -B "$ELF_FILE" | awk '(NR == 2){print $4}')

pbar_width=40
percent=$(((100 * $size_used + $TOTAL_CAPACITY/2) / $TOTAL_CAPACITY))
blocks=$((($pbar_width * $size_used + $TOTAL_CAPACITY/2) / $TOTAL_CAPACITY))
blocks_empty=$(($pbar_width-$blocks))

i=0
printf "SW MEMORY USAGE: ["
while [[ $i -lt $pbar_width ]]; do
	if [[ $i -lt $blocks ]]; then
		printf "#"
	else
		printf "-"
	fi
	i=$(($i + 1))
done
printf "] %u bytes of %u (%u%%)\n" $size_used "$TOTAL_CAPACITY" $percent

if [[ $# -ge 4 ]]; then
	touch "$4"
fi
