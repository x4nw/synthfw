#!/usr/bin/env python3
#
# (Originally from serv, modified by xan to pad)
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

import argparse
import sys

def int0(x):
    return int(x, 0)

class PadReader:
    def __init__(self, f, nbytes, pad):
        self.f = f
        self.nbytes = nbytes
        self.pad = bytes([pad])
        self.c = 0

    def read(self):
        b = self.f.read(1)
        if b:
            self.c += 1
            if self.nbytes >= 0 and self.c > self.nbytes:
                print("WARNING: binary file too long", file=sys.stderr)
            return b
        elif self.c < self.nbytes - 1:
            self.c += 1
            return self.pad
        else:
            return b''

ap = argparse.ArgumentParser(description="Convert a binary file to $readmem hex")
ap.add_argument("--len", type=int0, help="target length in words", default=-1)
ap.add_argument("--pad", type=int0, help="padding value", default=0)
ap.add_argument("-o", dest="out", help="output file")
ap.add_argument("file", help="file to process")
args = ap.parse_args()

if args.out:
    outf = open(args.out, "w")
else:
    outf = sys.stdout

with open(args.file, "rb") as f:
    cnt = 3
    s = ["00"]*4
    reader = PadReader(f, args.len * 4, args.pad)
    while True:
        data = reader.read()
        if not data:
            print(''.join(s), file=outf)
            exit(0)
        s[cnt] = "{:02X}".format(data[0])
        if cnt == 0:
            print(''.join(s), file=outf)
            s = ["00"]*4
            cnt = 4
        cnt -= 1
