#!/usr/bin/env python3
# Make bootloader packs for the SOC

import argparse
import binascii
import struct
import sys

HEADER_MAGIC = 0x484D4152

def int0(x):
    return int(x, 0)

# lol i should stop copying this around
def crc16_ccitt(crc, data):
    msb = crc >> 8
    lsb = crc & 255
    for c in data:
        x = c ^ msb
        x ^= (x >> 4)
        msb = (lsb ^ (x >> 3) ^ (x << 4)) & 255
        lsb = (x ^ (x << 5)) & 255
    return (msb << 8) + lsb

ap = argparse.ArgumentParser(description="make an SOC bootloader pack")
ap.add_argument("--gw", help="gateware")
ap.add_argument("--gwa", help="gateware start address", type=int0, default=0x00000000)
ap.add_argument("--sw", help="software")
ap.add_argument("--swa", help="software start address", type=int0, default=0x01000000)
ap.add_argument("--chunksz", help="chunk size", type=int0, default=128)
ap.add_argument("--erasesz", help="erase sector size", type=int0, default=64*1024)
ap.add_argument("--rmk", action="append", default=[], help="add a remark to the file")
ap.add_argument("-o", "--output", help="output filename")

def emit_file(fn, dest, addr, chunksize, erasesize, header):
    with open(fn, "rb") as f:
        data = f.read()

    if header:
        data = struct.pack("<II", HEADER_MAGIC, len(data)) + data

    n_erase = len(data)
    if n_erase % erasesize:
        n_erase += erasesize - (n_erase % erasesize)
    dest.write(f"E {addr:08X} {n_erase:08X}\n")

    pos = 0
    crc = 0xFFFF
    while pos < len(data):
        chunk = data[pos:pos + chunksize]
        crc = crc16_ccitt(crc, chunk)
        dest.write(f"W {addr + pos:08X} ")
        dest.write(binascii.hexlify(chunk).decode("ascii"))
        dest.write("\n")
        pos += len(chunk)

    dest.write(f"C {addr:08X} {len(data):08X} {crc:04X}\n")

args = ap.parse_args()

if args.output:
    of = open(args.output, "w")
else:
    of = sys.stdout

for i in args.rmk:
    of.write("# ")
    of.write(i)
    of.write("\n")

if args.gw:
    emit_file(args.gw, of, args.gwa, args.chunksz, args.erasesz, header=False)

if args.sw:
    emit_file(args.sw, of, args.swa, args.chunksz, args.erasesz, header=True)
