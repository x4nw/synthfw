#!/usr/bin/env python3
# build some gatewarez

import argparse
import os
import shlex
import subprocess
import sys

RED    = "\033[31m"
GREEN  = "\033[32m"
YELLOW = "\033[33m"
NORMAL = "\033[0;39m"

ap = argparse.ArgumentParser(description="build some gatewarez")
ap.add_argument("-I", action="append", dest="inc", default=[])
ap.add_argument("--view", action="store_true", help="view (sim) output")
ap.add_argument("--synth", action="store_true", help="synthesize")
ap.add_argument("--asc", action="store_true", help="stop at the .asc file")
ap.add_argument("--sim", action="store_true", help="build simulation")
ap.add_argument("--run", action="store_true", help="run simulation")
ap.add_argument("--freq", type=int, help="set freq (MHz)")
ap.add_argument("--fpga", help="set FPGA")
ap.add_argument("--package", help="set package")
ap.add_argument("--pcf", help="set PCF file")
ap.add_argument("--prefix", help="set prefix for names", default="out")
ap.add_argument("--dump", action="store_true", help="dump args and quit")
ap.add_argument("--trace-depth", help="verilator trace depth")
ap.add_argument("-D", action="append", dest="defs", default=[])
ap.add_argument("-v", action="store_true", dest="verbose", help="verbose")
ap.add_argument("-o", dest="out", help="output bitstream or fst file")
ap.add_argument("file", nargs="+")
args = ap.parse_args()

def vrun(command, **kwargs):
    if args.verbose:
        print(shlex.join(command))
    try:
        return subprocess.run(command, **kwargs)
    except subprocess.CalledProcessError as e:
        print(RED + shlex.join(command), file=sys.stderr)
        if e.output:
            sys.stderr.buffer.write(e.output)
        print(NORMAL, file=sys.stderr, flush=True)
        sys.exit(1)

def vpopen(command, **kwargs):
    if args.verbose:
        print(shlex.join(command))
    return subprocess.Popen(command, **kwargs)

if args.dump:
    print(args)
    sys.exit()

if args.synth:
    yosys_cmd = ["yosys"]
    if args.defs:
        yosys_cmd += [f"-D{d}" for d in args.defs]
    yosys_cmd += ["-p", f"synth_ice40 -dff -json \"{args.prefix}.json\""]
    yosys_cmd += [i for i in args.file if (i.endswith(".v") or i.endswith(".sv"))]

    yosys = vrun(yosys_cmd,
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
    with open(f"{args.prefix}.json.log", "wb") as f:
        f.write(yosys.stdout)

    nextpnr = vrun(
        ["nextpnr-ice40", f"--{args.fpga}", f"--package={args.package}",
         f"--freq={args.freq}", f"--pcf={args.pcf}",
         f"--json={args.prefix}.json", f"--asc={args.prefix}.asc"],
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
    with open(f"{args.prefix}.asc.log", "wb") as f:
        f.write(nextpnr.stdout)
    printing = False
    for line in nextpnr.stdout.decode("utf8").split("\n"):
        if line.startswith("Info: Device utilisation:"):
            printing = True
            print(GREEN, end="", file=sys.stderr)
        if printing and not line.strip():
            printing = False
        if printing:
            print(line, file=sys.stderr)
    print(NORMAL, file=sys.stderr, end="", flush=True)

    icetime = vrun(
        ["icetime", "-tmd", args.fpga, "-p", args.pcf, "-c", str(args.freq),
         f"{args.prefix}.asc"],
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
    with open(f"{args.prefix}.tim", "wb") as f:
        f.write(icetime.stdout)
    for line in icetime.stdout.decode("utf8").split("\n"):
        if line.startswith("Total path delay:"):
            print(GREEN + line + NORMAL, file=sys.stderr)

    if args.asc:
        if args.out:
            os.rename(f"{args.prefix}.asc", args.out)
        sys.exit(0)

    icepack = vrun(
        ["icepack", f"{args.prefix}.asc", f"{args.out}"],
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
    with open(f"{args.prefix}.pack.log", "wb") as f:
        f.write(icepack.stdout)

if args.sim:
    verilator_args = [
        "verilator", "--binary", "-j", "0", "-Wno-fatal", "-Werror-USERERROR",
         "--trace-fst",
         "--threads", "4", "-CFLAGS", "-std=c++14", "-o", f"ver.exe",
         "--Mdir", f"{args.prefix}"
    ]
    if args.trace_depth:
        verilator_args.extend(["--trace-depth", args.trace_depth])
    verilator_args.extend(f"-I{i}" for i in args.inc)
    verilator_args.extend(f"-D{d}" for d in args.defs)
    verilator_args.extend(args.file)

    verilator = vrun(verilator_args,
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
    with open(f"{args.prefix}/verilator.log", "wb") as f:
        f.write(verilator.stdout)
    printing = False
    for line in verilator.stdout.decode("utf8").split("\n"):
        if line.startswith("%"):
            printing = True
            if line.startswith("%Error"):
                print(RED, end="", file=sys.stderr)
            else:
                print(YELLOW, end="", file=sys.stderr)
        elif printing and not line.startswith(" "):
            printing = False
        if printing:
            print(line, file=sys.stderr)
    print(NORMAL, end="", flush=True, file=sys.stderr)

if args.run:
    run = vpopen(["stdbuf", "-o0", "-e0", f"./{args.prefix}/ver.exe"],
                 stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    with open(f"{args.prefix}/verilator.run.log", "wb") as f:
        while True:
            b = run.stdout.read(1)
            if b:
                sys.stdout.buffer.write(b)
                f.write(b)
            if b == b"\n":
                sys.stdout.buffer.flush()
            if not b and run.poll() is not None:
                break

    os.rename("dump.fst", f"{args.out}")

    if args.view:
        vrun(["gtkwave", f"{args.out}"])
