This repository contains the software, firmware, and gateware for
[my synth](https://gitlab.com/x4nw/synth) (excluding the system control
module, currently located at
[synth-scm-firmware](https://gitlab.com/x4nw/synth-scm-firmware) and
[synth-micropython](https://gitlab.com/x4nw/synth-micropython)).

