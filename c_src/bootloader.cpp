#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>

#include "rvcsr.hpp"
#include "accel.hpp"
#include "dbgout.hpp"
#include "rsi.hpp"

#include "board.hpp"

ACCEL_ENABLE;

__attribute__((interrupt))
void irq(void)
{
	uint32_t flags = SCTL.INTFLAGS;

	if (flags & SCTL_TIMER_IRQ)
		SCTL.INTFLAGS = 0x8000;

	if (flags & SCTL_UART0_IRQ)
	{
		uint8_t intid = UART0.IIR & UART_IIR_INTID_gm;
		uint8_t c;
		bool ok;

		switch (intid)
		{
		// Trying to cram into a tiny block RAM here - it'd be a good
		// idea to be able to handle all interrupts just in case one
		// gets accidentally enabled, but really, we don't need
		// handlers for ones that don't actually get turned on.
		/*case UART_IIR_INTID_LSI_gc:
			(void) UART0.LSR;
			break; */
		case UART_IIR_INTID_RBI_gc:
		case UART_IIR_INTID_TMOI_gc:
			while (UART0.LSR & (UART_LSR_DR | UART_LSR_RXFIFOE))
			{
				c = UART0.RBR;
				ok = rsi_handle_byte(c);
				if (!ok) break;
			}

			if (!ok)
			{
				UART0.IER = 0;
			}
			break;
		/*case UART_IIR_INTID_TBEI_gc:
			// Already cleared by reading IIR.
			break;*/
		/*case UART_IIR_INTID_DSSI_gc:
			(void) UART0.MSR;
			break;*/
		}
	}
}

void rsi_transmit_byte(uint8_t data)
{
	while (!(UART0.LSR & UART_LSR_THRE)) ;
	UART0.THR = data;
}

void rsi_enable_rx(void)
{
	UART0.FCR = UART_FCR_FIFOEN | UART_FCR_RXFIFTL_8_gc | UART_FCR_RXCLR;
	UART0.FCR = UART_FCR_FIFOEN | UART_FCR_RXFIFTL_8_gc;
	UART0.IER = UART_IER_ERBI;
}

uint16_t rsi_crc16_ccitt_update(uint16_t crc, uint8_t b)
{
	uint32_t c = crc16_ccitt_update(crc, b);
	// This is completely pointless but it saves two instructions which
	// makes me happy
	if (c & 0xFFFF0000) __builtin_unreachable();
	return c;
}

void rsi_disable_irq()
{
	rv_irq_disable();
}

void rsi_enable_irq()
{
	rv_irq_enable();
}

int main(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

	GPIO0.DIRSET = PIN_WDI;
	GPIO0.OUTTGL = PIN_WDI;
	GPIO0.OUTTGL = PIN_WDI;

	rv_set_handler(irq);
	rv_irq_enable();
	rv_exc_enable();
	SCTL.DIVISOR = 15999;

	uint16_t divisor = F_PER / (16 * F_BAUD);
	UART0.LCR =
		UART_LCR_WLS_7BIT_gc
		| UART_LCR_STB_2_gc
		| UART_LCR_PARITY_EVEN_gc;
	UART0.set_divisor_reg(divisor);
	UART0.FCR = UART_FCR_FIFOEN | UART_FCR_RXFIFTL_8_gc;
	UART0.MCR = 0;
	UART0.IER = UART_IER_ERBI;

	SCTL.INTEN = SCTL_TIMER_IRQ | SCTL_UART0_IRQ;

	GPIO0.OUTSET = PIN_FLASH_nCS;
	GPIO0.DIRSET = PIN_FLASH_nCS;
	// 500 kHz is about right for this painfully slow CPU :)
	// It'd be nice to raise this to 1 MHz at least, but we need to loop
	// faster. Wait until we're actually at the point of performing a boot
	// then try to optimize that.
	SSPI0.init(SSPI_CDIV_128, 0);

	rsi_init();

	dbgout("System up\n");

	for (;;)
	{
		rsi_process();
		GPIO0.OUTTGL = PIN_WDI;
	}
}
